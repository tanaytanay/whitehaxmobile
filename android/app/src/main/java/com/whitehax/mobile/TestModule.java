package com.whitehax.mobile;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.whitehax.aujas.security.securityhandlers.GeneralCheckHandler;
import com.whitehax.aujas.security.securityhandlers.WifiManagerHandler;

import android.app.Activity;
import android.util.Log;


public class TestModule extends ReactContextBaseJavaModule {
    String googlePlayStatus = "FAIL";
    private final ReactApplicationContext reactContext;
    //private final ReactApplicationContext baseContext;
    //private final Activity activity;
    public TestModule(ReactApplicationContext reactContext) {
        super(reactContext); //required by React Native
        this.reactContext = reactContext;
        //this.baseContext = getReactApplicationContext();
        //this.activity = getCurrentActivity();
    }


    

    @Override
    //getName is required to define the name of the module represented in JavaScript

    public String getName() {
        return "TestModule";
    }




    

    @ReactMethod
    public void getString(Callback success) {
        SafetyNet.getClient(this.reactContext)
                .isVerifyAppsEnabled()
                .addOnCompleteListener(new OnCompleteListener<SafetyNetApi.VerifyAppsUserResponse>() {
                    @Override
                    public void onComplete(Task<SafetyNetApi.VerifyAppsUserResponse> task) {
                        if (task.isSuccessful()) {
                            SafetyNetApi.VerifyAppsUserResponse result = task.getResult();
                            if (result.isVerifyAppsEnabled()) {
                                googlePlayStatus= "PASS";
                            } else {
                                Log.d("MY_APP_TAG", "The Verify Apps feature is disabled.");
                            }
                        } else {
                            Log.e("MY_APP_TAG", "A general error occurred.");
                        }
                    }
                });

        GeneralCheckHandler objGeneral = new GeneralCheckHandler(this.reactContext);


        success.invoke(objGeneral.getResult());
    }



    @ReactMethod
    public void getWifi(Callback success) {

        WifiManagerHandler objWifi = new WifiManagerHandler(this.reactContext);

        success.invoke(objWifi.getResult());
    }

    @ReactMethod
    public void getGooglePlayProtect(Callback success) {

        success.invoke(googlePlayStatus);
    }

}