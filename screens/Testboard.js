import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { PixelRatio, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
//import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Dimensions } from 'react-native';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Home from './Dashboard.js';
import Devices from './Devices.js';
import Remediation from './Remediation.js';
import Details from './DetailsPage.js';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DeviceInfo from 'react-native-device-info';


import AppContext from "../components/AppContext";


import {NativeModules} from 'react-native';



var TestModule = NativeModules.TestModule;

const windowWidth = Dimensions.get('screen').width;

const windowHeight = Dimensions.get('screen').height;
let bordCol = 'white'
let bordWid = 0

const Stack = createStackNavigator();

const Tab = createMaterialTopTabNavigator();

function HeaderComponent(){
  return(
    
    <Text style= {{ fontSize: windowHeight/30, color:'#FFFFFF' }}>White<Text style= {{ fontSize: windowHeight/30, color:'#FE0000' }}>HaX</Text></Text>
    
  )
}
function MyTabs({navigation}) {


  const myContext = useContext(AppContext);

  const [RunModalVisibleValue, setRunModalVisible] = useState(false)
  const [WifiModalVisibleValue, setWifiModalVisible] = useState(false)
  const [SyncModalVisibleValue, setSyncModalVisible] = useState(false)

  const [RunCloseButton, setRunCloseButton] = useState(true)
  const [SyncCloseButton, setSyncCloseButton] = useState(true)
  const [WifiCloseButton, setWifiCloseButton] = useState(true)

  const [RunProg, setRunProg] = useState(0)
  const [SyncProg, setSyncProg] = useState(0)
  const [WifiProg, setWifiProg] = useState(0)

  const [RunCloseButtonColor, setRunCloseButtonColor] = useState('gray')
  const [SyncCloseButtonColor, setSyncCloseButtonColor] = useState('gray')
  const [WifiCloseButtonColor, setWifiCloseButtonColor] = useState('gray')



  const D = new Date();

                          



  function getTestValue() {
    myContext.setNavigationVar(navigation);
    myContext.setRunStatus("Running General Checks...");
    setRunCloseButton(true)

    setRunProg((state) => {
      console.log(state); 
      return state+10;
    });
    
    

    //findgeneral score variables used for both wifi and non wifi-->
    let tempCtr = 0;
    let debugMode = true;
    console.log("test value called")

    let configJSON = {"profile_subcategory_mappings":[{"id":4001,"test_name":"lockPasswordEnable","attack_name":"WH_DeviceLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Device Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4002,"test_name":"","attack_name":"WH_SmartLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":6,"readiness_type":"Device","display_name":"Smart Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4003,"test_name":"isLockPasswordStrong","attack_name":"WH_Passwd_Check","estimated_time":30,"decription":"You should use a strong passwd for Device and Data protection","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Weak Password","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4004,"test_name":"isGooglePlayProtectEnable","attack_name":"WH_Google_Play_Protect","estimated_time":30,"decription":"Google Play Protect provides application verification","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Application Verification","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4005,"test_name":"","attack_name":"WH_SW_Version_Check","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","remedy_str":"NA","weight":25,"readiness_type":"Device","display_name":"Android OS Version","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4006,"test_name":"","attack_name":"WH_SW_Update_ON","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","weight":5,"readiness_type":"Device","display_name":"Android OS Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4007,"test_name":"defaultBrowserSafeCheck","attack_name":"WH_Browser_Default","estimated_time":30,"decription":"Use a browser that provides strong privacy and data protection","security_soln":"Web Security","weight":7,"readiness_type":"Web","display_name":"Browser Type","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4008,"test_name":"defaultBrowserUpdated","attack_name":"WH_Browser_Version","estimated_time":30,"decription":"Keep you Browser up-to-date to get latest security features","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Broswer Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4009,"test_name":"","attack_name":"WH_Safe_Browsing_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Safe Browser","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4010,"test_name":"webProxy","attack_name":"WH_Web_Proxy_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Web Proxy for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4011,"test_name":"","attack_name":"WH_Web_VPN_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Web VPN for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4012,"test_name":"","attack_name":"WH_2FA_Auth_Check","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Device","display_name":"2 Factor Authentication","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4013,"test_name":"","attack_name":"WH_Google_Account_Security","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Data","display_name":"Google Account Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4014,"test_name":"camerLoctionPermission","attack_name":"WH_Privacy_Loc_Share_Cam","estimated_time":30,"decription":"Turn off Location Sharing when using Camera to protect privacy","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Camera Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4015,"test_name":"isAppLocationSharingPermission","attack_name":"WH_Privacy_Loc_Share_Apps","estimated_time":30,"decription":"Turn off Location Sharing when using Social Media to protect privacy","security_soln":"Privacy","weight":5,"readiness_type":"Privacy","display_name":"Privacy - App Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4016,"test_name":"isAppLocationSharingSocialPermission","attack_name":"WH_Privacy_SocMed_Loc_Share","estimated_time":30,"decription":"Turn on Privacy and security settings when using Social Media Apps","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Social Media Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4017,"test_name":"weakFireWallApp","attack_name":"WH_FW_Installed","estimated_time":30,"decription":"Firewall protects your Device communication","security_soln":"Firewall","weight":8,"readiness_type":"Firewall","display_name":"Weak Firewall","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4018,"test_name":"antiPhishingApp","attack_name":"WH_AntiPhishing_Installed","estimated_time":30,"decription":"You are missing Email protection against Phishing ","security_soln":"Email","weight":6,"readiness_type":"Email","display_name":"Weak Email Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4019,"test_name":"antiVirusAppCheck","attack_name":"WH_AntiVirus_Installed","estimated_time":30,"decription":"You are missing  protection against Viruses and Malware ","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Weak Virus Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4020,"test_name":"vPNAppAvailable","attack_name":"WH_VPN_App_Check","estimated_time":30,"decription":"You are missing  VPN for enhance privacy and security while browsing Admin User access can cause many types of data and security breach","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Blocking ISP Data Snooping","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4021,"test_name":"","attack_name":"WH_DataExfiltration_Cookie_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":7,"readiness_type":"Data","display_name":"Data Stealing thru Cookies","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4022,"test_name":"","attack_name":"WH_DataExfiltration_Dir_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":5,"readiness_type":"Data","display_name":"Data Stealing thru Files","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4023,"test_name":"deviceRootcheck1","attack_name":"WH_OS_Rooted_Check1","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4024,"test_name":"deviceRootcheck2","attack_name":"WH_OS_Rooted_Check2","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4025,"test_name":"deviceRootcheck3","attack_name":"WH_OS_Rooted_Check3","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4027,"test_name":"isHiddenSSID","attack_name":"WH_Wifi_SSID_Visible","estimated_time":30,"decription":"Hide WiFi SSID to protect your WiFi against intrusions","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi SSID Pubicly Visible","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4028,"test_name":"isWifiEncrypted","attack_name":"WH_Wifi_Encrypt_Strength","estimated_time":30,"decription":"Use strong WiFI Encryption for stronger security","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"Weak WiFi Encryption","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4029,"test_name":"isIpDefaultRouter","attack_name":"WH_Wifi_Router_IP_Default","estimated_time":30,"decription":"Using vendor default IP increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"WiFi Security","display_name":"WiFi uses Vendor Default","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4030,"test_name":"isRouterAdminAccess","attack_name":"WH_Wifi_Router_Admin_Access","estimated_time":30,"decription":"Using vendor default Router passwd increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"WiFi Admin Access Break-in","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4031,"test_name":"isport1Available","attack_name":"WH_Wifi_FW_Port1","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"FirFirewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4032,"test_name":"isport2Available","attack_name":"WH_Wifi_FW_Port2","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4033,"test_name":"isport3Available","attack_name":"WH_Wifi_FW_Port3","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4034,"test_name":"isport4Available","attack_name":"WH_Wifi_FW_Port4","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4035,"test_name":"dnsPos","attack_name":"WH_Wifi_DNS_Poisoning","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi DNS Poisoning Risk","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4036,"test_name":"eicarFileCheck","attack_name":"WH_EICAR","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4037,"test_name":"eicarFileCheck1","attack_name":"WH_EICAR1","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4038,"test_name":"eicarFileCheck2","attack_name":"WH_EICAR2","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4039,"test_name":"eicarFileCheck3","attack_name":"WH_EICAR3","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4040,"test_name":"eicarFileCheck4","attack_name":"WH_EICAR4","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4041,"test_name":"eicarFileCheck5","attack_name":"WH_EICAR5","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4042,"test_name":"vpnNat","attack_name":"WH_Wifi_VPN_NAT","estimated_time":30,"decription":"You VPN over WiFi to increase Data Security and Privacy","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"No WiFi VPN","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}}]}
    let totalWeight = 0;
    let passedWeight = 0;
    let failedWeight = 0;

    let NumCritTests = 0;
    let NumHighTests = 0;
    let NumMedTests = 0;
    let NumLowTests = 0;

    let DevicePassWeight = 0;
    let EmailPassWeight = 0;
    let WebPassWeight = 0;
    let WifiPassWeight = 0;
    let FirewallPassWeight = 0;
    let DataPassWeight = 0;
    let PrivacyPassWeight = 0;

    let DeviceWeight = 0;
    let EmailWeight = 0;
    let WebWeight = 0;
    let WifiWeight = 0;
    let FirewallWeight = 0;
    let DataWeight = 0;
    let PrivacyWeight = 0;

    let passGeneralVal = 0;
    let failGeneralVal = 0;

                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });
    


    let i,j;

    //<-- findgeneral score variables used for both wifi and non wifi

    TestModule.getString((info) => {
                                                  myContext.setRunStatus("Running WiFi Checks...");

                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });
                          

      
      console.log(info)
            myContext.setGeneralTestInfo(JSON.parse(info));
            
            myContext.setTotalGeneralTests(JSON.parse(info).generalChecks.length + 1)
            
            
            //let locationPermFound = false;
            
            //let i, j;
            for (i = 0; i < JSON.parse(info).generalChecks.length; i++) {
              if (JSON.parse(info).generalChecks[i].status == "PASS") {
                passGeneralVal++;
              }
              else {
                failGeneralVal++;
              }
            }
            
            //console.log(passVal)
            

          
          //findGeneralScore(); -->

          
            //non wifi -->

            let TopGeneral = [0,0,0,0];
            let TopGeneralIndex = [0,0,0,0];

                           

            for (i = 0; i <JSON.parse(info).generalChecks.length; i++) {
              totalWeight += JSON.parse(info).generalChecks[i].weight;
              
              
              if (JSON.parse(info).generalChecks[i].readinessType[0] == "Device") {
                DeviceWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Email") {
                EmailWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Web") {
                WebWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "WiFi Security") {
                WifiWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Firewall") {
                FirewallWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Data") {
                DataWeight += JSON.parse(info).generalChecks[i].weight;
              }
              else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Privacy") {
                PrivacyWeight += JSON.parse(info).generalChecks[i].weight;
              }


              

              


              if (JSON.parse(info).generalChecks[i].status == "PASS") {
                passedWeight += JSON.parse(info).generalChecks[i].weight;

                if (JSON.parse(info).generalChecks[i].readinessType[0] == "Device") {
                  DevicePassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setDeviceReadinessWeightValue(myContext.DeviceReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Email") {
                  EmailPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setEmailReadinessWeightValue(++ myContext.EmailReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Web") {
                  WebPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setWebReadinessWeightValue(++ myContext.WebReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "WiFi Security") {
                  WifiPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setWifiReadinessWeightValue(++ myContext.WifiReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Firewall") {
                  FirewallPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setFirewallReadinessWeightValue(++ myContext.FirewallReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Data") {
                  DataPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setDataReadinessWeightValue(++ myContext.DataReadinessWeightValue)
                }
                else if (JSON.parse(info).generalChecks[i].readinessType[0] == "Privacy") {
                  PrivacyPassWeight += JSON.parse(info).generalChecks[i].weight;
                  // myContext.setDataReadinessWeightValue(++ myContext.DataReadinessWeightValue)
                }
              }

              else {

                if (JSON.parse(info).generalChecks[i].weight > TopGeneral[0]) {
                  TopGeneral[3] = TopGeneral[2];
                  TopGeneral[2] = TopGeneral[1];
                  TopGeneral[1] = TopGeneral[0];
                  TopGeneral[0] = JSON.parse(info).generalChecks[i].weight;

                  TopGeneralIndex[3] = TopGeneralIndex[2];
                  TopGeneralIndex[2] = TopGeneralIndex[1];
                  TopGeneralIndex[1] = TopGeneralIndex[0];
                  TopGeneralIndex[0] = i;
                }
                else if (JSON.parse(info).generalChecks[i].weight > TopGeneral[1]) {
                  TopGeneral[2] = TopGeneral[1];
                  TopGeneral[1] = TopGeneral[0];
                  TopGeneral[0] = JSON.parse(info).generalChecks[i].weight;

                  TopGeneralIndex[2] = TopGeneralIndex[1];
                  TopGeneralIndex[1] = TopGeneralIndex[0];
                  TopGeneralIndex[0] = i;
                }
                else if (JSON.parse(info).generalChecks[i].weight > TopGeneral[2]) {
                  TopGeneral[1] = TopGeneral[0];
                  TopGeneral[0] = JSON.parse(info).generalChecks[i].weight;

                  TopGeneralIndex[1] = TopGeneralIndex[0];
                  TopGeneralIndex[0] = i;
                }
                else if (JSON.parse(info).generalChecks[i].weight > TopGeneral[3]) {
                  TopGeneral[0] = JSON.parse(info).generalChecks[i].weight;

                  TopGeneralIndex[0] = i;
                }

                failedWeight += JSON.parse(info).generalChecks[i].weight;
                if (JSON.parse(info).generalChecks[i].weight >= 9)
                  NumCritTests ++ ;
                else if (JSON.parse(info).generalChecks[i].weight >= 6)
                  NumHighTests ++ ;
                else if (JSON.parse(info).generalChecks[i].weight >= 4)
                  NumMedTests ++ ;
                else
                  NumLowTests ++ ;
              }

                             
            

            }

            //remediation stuff -->

            

            let OneRemName = ''
            let OneRemDescription = '';
            let TwoRemName = ''
            let TwoRemDescription = '';
            let ThreeRemName = ''
            let ThreeRemDescription = '';
            let FourRemName = ''
            let FourRemDescription = '';

            let OneRemSeverity = '';
            let TwoRemSeverity = '';
            let ThreeRemSeverity = '';
            let FourRemSeverity = '';


            for (i = 0; i < configJSON.profile_subcategory_mappings.length; i ++) {
              if ( JSON.parse(info).generalChecks[TopGeneralIndex[0]].name == configJSON.profile_subcategory_mappings[i].test_name ) {
                OneRemName = configJSON.profile_subcategory_mappings[i].display_name
                OneRemDescription = configJSON.profile_subcategory_mappings[i].decription
                OneRemSeverity = configJSON.profile_subcategory_mappings[i].weight
                // console.log(configJSON.profile_subcategory_mappings[i].display_name)
                // console.log(configJSON.profile_subcategory_mappings[i].decription)
              }
              if ( JSON.parse(info).generalChecks[TopGeneralIndex[1]].name == configJSON.profile_subcategory_mappings[i].test_name ) {
                TwoRemName = configJSON.profile_subcategory_mappings[i].display_name
                TwoRemDescription = configJSON.profile_subcategory_mappings[i].decription
                TwoRemSeverity = configJSON.profile_subcategory_mappings[i].weight

                // console.log(configJSON.profile_subcategory_mappings[i].display_name)
                // console.log(configJSON.profile_subcategory_mappings[i].decription)
              }
              if ( JSON.parse(info).generalChecks[TopGeneralIndex[2]].name == configJSON.profile_subcategory_mappings[i].test_name ) {
                ThreeRemName = configJSON.profile_subcategory_mappings[i].display_name
                ThreeRemDescription = configJSON.profile_subcategory_mappings[i].decription
                ThreeRemSeverity = configJSON.profile_subcategory_mappings[i].weight

                // console.log(configJSON.profile_subcategory_mappings[i].display_name)
                // console.log(configJSON.profile_subcategory_mappings[i].decription)
              }
              if ( JSON.parse(info).generalChecks[TopGeneralIndex[3]].name == configJSON.profile_subcategory_mappings[i].test_name ) {
                FourRemName = configJSON.profile_subcategory_mappings[i].display_name
                FourRemDescription = configJSON.profile_subcategory_mappings[i].decription
                FourRemSeverity = configJSON.profile_subcategory_mappings[i].weight

                // console.log(configJSON.profile_subcategory_mappings[i].display_name)
                // console.log(configJSON.profile_subcategory_mappings[i].decription)
              }
            }
                           

            myContext.setOneRemName(OneRemName);
            myContext.setOneRemDescription(OneRemDescription);
           
            myContext.setTwoRemName(TwoRemName);
            myContext.setTwoRemDescription(TwoRemDescription);
           
            myContext.setThreeRemName(ThreeRemName);
            myContext.setThreeRemDescription(ThreeRemDescription);
           
            myContext.setFourRemName(FourRemName);
            myContext.setFourRemDescription(FourRemDescription);

            myContext.setOneRemSeverity(OneRemSeverity);
            myContext.setTwoRemSeverity(TwoRemSeverity);
            myContext.setThreeRemSeverity(ThreeRemSeverity);
            myContext.setFourRemSeverity(FourRemSeverity);

                
            

            myContext.setOneRemSteps(JSON.parse(info).generalChecks[TopGeneralIndex[0]].remediationStep)
            myContext.setTwoRemSteps(JSON.parse(info).generalChecks[TopGeneralIndex[1]].remediationStep)
            myContext.setThreeRemSteps(JSON.parse(info).generalChecks[TopGeneralIndex[2]].remediationStep)
            myContext.setFourRemSteps(JSON.parse(info).generalChecks[TopGeneralIndex[3]].remediationStep)

            //<-- remediation stuff 


            //<-- non wifi

                           


            //<--findGeneralScore();

            console.log('non wifi tests done')
                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });
                          


    });
                             
    TestModule.getGooglePlayProtect((GPPinfo) => {

                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });

        console.log("GPPinfo "+GPPinfo)
      if (GPPinfo == "PASS") {
        console.log("pass")
        passGeneralVal++;
        DevicePassWeight+=7;
      }
      else  {
        console.log("fail")
        failGeneralVal++;
        failedWeight += 7;
        NumHighTests++;
      }

      DeviceWeight += 7;
      
      console.log("pass gen test: "+passGeneralVal)

      myContext.setPassedGeneralTests(passGeneralVal)
      myContext.setFailedGeneralTests(failGeneralVal)
                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });

      
      
    });
    TestModule.getWifi((info) => {
                                                              //myContext.setRunStatus("Compili...");

      
                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });

           

      //console.log(info)
      //console.log("x" == "x" ? 'tru' : 'fal')
      myContext.setWifiTestInfo(JSON.parse(info));
      myContext.setTotalWifiTests(JSON.parse(info).wifiCheck.length)
      myContext.setSSID(JSON.parse(info).ssid)
      
      //console.log(Object.values(JSON.parse(info))[1])
      
      let i,j;
      let passVal = 0;
      let failVal = 0;
                           

      for (i = 0; i < JSON.parse(info).wifiCheck.length; i++) {
             
      

        if (JSON.parse(info).wifiCheck[i].value == "PASS") {
          passVal++;
        }
        else {
          failVal++;
        }
      }
      myContext.setPassedWifiTests(passVal);
      myContext.setFailedWifiTests(failVal);


                           

      //findgeneralscore () ->>

      // wifi -->>


                let NumCritWifiTests = 0;
                let NumHighWifiTests = 0;
                let NumMedWifiTests = 0;
                let NumLowWifiTests = 0;

                


                

                let PasswordWifiPassWeight = 0;
                let EncryptionWifiPassWeight = 0;
                let FirewallWifiPassWeight = 0;
                let DnsWifiPassWeight = 0;
                let EavesdroppingWifiPassWeight = 0;

                let PasswordWifiWeight = 0;
                let EncryptionWifiWeight = 0;
                let FirewallWifiWeight = 0;
                let DnsWifiWeight = 0;
                let EavesdroppingWifiWeight = 0;


                for (i = 0; i < JSON.parse(info).wifiCheck.length; i ++) {
                  totalWeight += JSON.parse(info).wifiCheck[i].weight;

                  if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Device") {
                    DeviceWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Email") {
                    EmailWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Web") {
                    WebWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "WiFi Security") {
                    WifiWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Firewall") {
                    FirewallWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Data") {
                    DataWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Privacy") {
                    PrivacyWeight += JSON.parse(info).wifiCheck[i].weight;
                  }

                  if (JSON.parse(info).wifiCheck[i].value == "PASS") {
                    passedWeight += JSON.parse(info).wifiCheck[i].weight;

                    if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Device") {
                      DevicePassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setDeviceReadinessWeightWeight(myContext.DeviceReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Email") {
                      EmailPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setEmailReadinessWeightWeight(++ myContext.EmailReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Web") {
                      WebPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setWebReadinessWeightWeight(++ myContext.WebReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "WiFi Security") {
                      WifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setWifiReadinessWeightWeight(++ myContext.WifiReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Firewall") {
                      FirewallPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setFirewallReadinessWeightWeight(++ myContext.FirewallReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Data") {
                      DataPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setDataReadinessWeightWeight(++ myContext.DataReadinessWeightWeight)
                    }
                    else if (JSON.parse(info).wifiCheck[i].readinessType[0] == "Privacy") {
                      PrivacyPassWeight += JSON.parse(info).wifiCheck[i].weight;
                      // myContext.setDataReadinessWeightWeight(++ myContext.DataReadinessWeightWeight)
                    }
                  }
                  else {

                    // if (JSON.parse(info).wifiCheck[i].weight > TopWifi[0]) {
                    //   TopWifi[0] = JSON.parse(info).wifiCheck[i].weight;
                    //   TopWifiIndex[0] = i;
                    // }
                    // else if (JSON.parse(info).wifiCheck[i].weight > TopWifi[1]) {
                    //   TopWifi[1] = JSON.parse(info).wifiCheck[i].weight;
                    //   TopWifiIndex[1] = i;
                    // }
                    // else if (JSON.parse(info).wifiCheck[i].weight > TopWifi[2]) {
                    //   TopWifi[2] = JSON.parse(info).wifiCheck[i].weight;
                    //   TopWifiIndex[2] = i;
                    // }
                    // else if (JSON.parse(info).wifiCheck[i].weight > TopWifi[3]) {
                    //   TopWifi[3] = JSON.parse(info).wifiCheck[i].weight;
                    //   TopWifiIndex[3] = i;
                    // }


                    failedWeight += JSON.parse(info).wifiCheck[i].weight;
                    if (JSON.parse(info).wifiCheck[i].weight >= 9)
                      NumCritTests ++ ;
                    else if (JSON.parse(info).wifiCheck[i].weight >= 6)
                      NumHighTests ++ ;
                    else if (JSON.parse(info).wifiCheck[i].weight >= 4)
                      NumMedTests ++ ;
                    else
                      NumLowTests ++ ;
                  }      

                }

                //wifi check -->
                let totalWifiWeight = 0;
                let passedWifiWeight = 0;
                let failedWifiWeight = 0;
                //console.log(tempJSON);
                               
            
               
                
                for (i = 0; i <JSON.parse(info).wifiCheck.length; i++) {
                  totalWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                  if (JSON.parse(info).wifiCheck[i].value == "PASS") {
                    passedWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                  }
                  else{
                    failedWifiWeight += JSON.parse(info).wifiCheck[i].weight;

                    if (JSON.parse(info).wifiCheck[i].weight >= 9)
                      NumCritWifiTests++;
                    else if (JSON.parse(info).wifiCheck[i].weight >= 6)
                      NumHighWifiTests++;
                    else if (JSON.parse(info).wifiCheck[i].weight >= 4)
                      NumMedWifiTests++;
                    else
                      NumLowWifiTests++;
                  }
                }

                //wifi number of failed tests critical, high, med, low:
                myContext.setCritWifiTests(NumCritWifiTests);
                myContext.setHighWifiTests(NumHighWifiTests);
                myContext.setMedWifiTests(NumMedWifiTests);
                myContext.setLowWifiTests(NumLowWifiTests);
                //
                
                myContext.setTotalWifiWeight(totalWifiWeight);
                myContext.setFailedWifiWeight(failedWifiWeight);
                myContext.setReadinessWifiPercent(Math.round(passedWifiWeight/totalWifiWeight * 100))

                if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 67) {
                      myContext.setReadinessWifiTitle("Low");
                      myContext.setReadinessWifiColor("#2E7D32");
                    }
                else if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 33) {
                      myContext.setReadinessWifiTitle("Medium");
                      myContext.setReadinessWifiColor("#F57F17");
                    }
                else if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 15) {
                      myContext.setReadinessWifiTitle("High");
                      myContext.setReadinessWifiColor("#E65100");
                    }
                else {
                      myContext.setReadinessWifiTitle("Critical") ;
                      myContext.setReadinessWifiColor("#B71C1C");
                    }

                //<--wifi check

                //wifi table ->>
                           

                for ( i = 0; i < JSON.parse(info).wifiCheck.length ; i++) {
                  
                  if ( JSON.parse(info).wifiCheck[i].value == "PASS") {
                    
                    if ( JSON.parse(info).wifiCheck[i].name == 'isHiddenSSID') {
                      PasswordWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                      PasswordWifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isWifiEncrypted') {
                      EncryptionWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                      EncryptionWifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isport1Available' || JSON.parse(info).wifiCheck[i].name == 'isport2Available' || JSON.parse(info).wifiCheck[i].name == 'isport3Available' || JSON.parse(info).wifiCheck[i].name == 'isport4Available' ) {
                      FirewallWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                      FirewallWifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'dnsPos') {
                      DnsWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                      DnsWifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isIpDefaultRouter' || JSON.parse(info).wifiCheck[i].name == 'isRouterAdminAccess' || JSON.parse(info).wifiCheck[i].name == 'isVpnNat') {
                      EavesdroppingWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                      EavesdroppingWifiPassWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                  }
                  else{
                    if ( JSON.parse(info).wifiCheck[i].name == 'isHiddenSSID') {
                      PasswordWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isWifiEncrypted') {
                      EncryptionWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isport1Available' || JSON.parse(info).wifiCheck[i].name == 'isport2Available' || JSON.parse(info).wifiCheck[i].name == 'isport3Available' || JSON.parse(info).wifiCheck[i].name == 'isport4Available' ) {
                      FirewallWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'dnsPos') {
                      DnsWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                    if ( JSON.parse(info).wifiCheck[i].name == 'isIpDefaultRouter' || JSON.parse(info).wifiCheck[i].name == 'isRouterAdminAccess' || JSON.parse(info).wifiCheck[i].name == 'isVpnNat') {
                      EavesdroppingWifiWeight += JSON.parse(info).wifiCheck[i].weight;
                    }

                  }
                  
                }
                           

                //<--wifi table 
                myContext.setPasswordWifiPercentBar(Math.round(PasswordWifiPassWeight/PasswordWifiWeight * 100))
                myContext.setEncryptionWifiPercentBar(Math.round(EncryptionWifiPassWeight/EncryptionWifiWeight * 100))
                myContext.setFirewallWifiPercentBar(Math.round(FirewallWifiPassWeight/FirewallWifiWeight * 100))
                myContext.setDnsWifiPercentBar(Math.round(DnsWifiPassWeight/DnsWifiWeight * 100))
                myContext.setEavesdroppingWifiPercentBar(Math.round(EavesdroppingWifiPassWeight/EavesdroppingWifiWeight * 100))


      //<-- wifi

      //<-- findgeneralscore ()



                    console.log("wifi tests done")



                    //setting state after both non wifi and wifi checks are done -->
                  console.log("setting state")

                  if (DeviceWeight == 0) DeviceWeight+=1;
                  if (WifiWeight == 0) WifiWeight+=1;
                  if (DataWeight == 0) DataWeight+=1;
                  if (EmailWeight == 0) EmailWeight+=1;
                  if (WebWeight == 0) WebWeight+=1;
                  if (FirewallWeight == 0) FirewallWeight+=1;
                  if (PrivacyWeight == 0) PrivacyWeight+=1;
                 
                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });
                                          
                 
                  myContext.setTotalGeneralWeight(totalWeight);
                  myContext.setFailedGeneralWeight(failedWeight);
                  myContext.setReadinessGeneralPercent(Math.round(passedWeight/totalWeight * 100))

                  if (Math.round(passedWeight/totalWeight * 100) >= 67) {
                    myContext.setReadinessGeneralColor("#2E7D32")
                    myContext.setReadinessGeneralTitle("Low")
                  }
                  else if (Math.round(passedWeight/totalWeight * 100) >= 33) {
                    myContext.setReadinessGeneralColor("#F57F17")
                    myContext.setReadinessGeneralTitle("Medium")
                  }
                  else if (Math.round(passedWeight/totalWeight * 100) >= 15) {
                    myContext.setReadinessGeneralColor("#E65100")
                    myContext.setReadinessGeneralTitle("Medium")
                  }
                  else {
                    myContext.setReadinessGeneralColor("#B71C1C")
                    myContext.setReadinessGeneralTitle("Critical")
                  }

                  myContext.setCritGeneralTests(NumLowTests);
                  myContext.setHighGeneralTests(NumMedTests);
                  myContext.setMedGeneralTests(NumHighTests);
                  myContext.setLowGeneralTests(NumCritTests);

                  myContext.setDeviceReadinessPercent(Math.round(DevicePassWeight/totalWeight * 100) == 0 ? 1 : Math.round(DevicePassWeight/totalWeight * 100))
                  myContext.setActiveButtonValue(Math.round(DevicePassWeight/totalWeight * 100) == 0 ? 1 : Math.round(DevicePassWeight/totalWeight * 100))
                  if (Math.round(DevicePassWeight/DeviceWeight * 100) > 67) {
                    myContext.setActiveButtonRiskValue("Low")
                    myContext.setActiveButtonRiskValueColor("#2E7D32")
                  }
                  else if (Math.round(DevicePassWeight/DeviceWeight * 100) > 33) {
                    myContext.setActiveButtonRiskValue("Medium")
                    myContext.setActiveButtonRiskValueColor("#F57F17")
                  }
                  else if (Math.round(DevicePassWeight/DeviceWeight * 100) > 15) {
                    myContext.setActiveButtonRiskValue("High")
                    myContext.setActiveButtonRiskValueColor("#E65100")
                  }
                  else {
                    myContext.setActiveButtonRiskValue("Crit")
                    myContext.setActiveButtonRiskValueColor("#B71C1C")
                  }
                  myContext.setEmailReadinessPercent(Math.round(EmailPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(EmailPassWeight/totalWeight * 100))
                  myContext.setWebReadinessPercent(Math.round(WebPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(WebPassWeight/totalWeight * 100))
                  myContext.setWifiReadinessPercent(Math.round(WifiPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(WifiPassWeight/totalWeight * 100))
                  myContext.setFirewallReadinessPercent(Math.round(FirewallPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(FirewallPassWeight/totalWeight * 100))
                  myContext.setDataReadinessPercent(Math.round(DataPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(DataPassWeight/totalWeight * 100))    
                  myContext.setPrivacyReadinessPercent(Math.round(PrivacyPassWeight/totalWeight * 100) == 0 ? 1 : Math.round(PrivacyPassWeight/totalWeight * 100))    

                  myContext.setDeviceReadinessPercentBar(Math.round(DevicePassWeight/DeviceWeight * 100))
                  myContext.setEmailReadinessPercentBar(Math.round(EmailPassWeight/EmailWeight * 100))
                  myContext.setWebReadinessPercentBar(Math.round(WebPassWeight/WebWeight * 100))
                  myContext.setWifiReadinessPercentBar(Math.round(WifiPassWeight/WifiWeight * 100))
                  myContext.setFirewallReadinessPercentBar(Math.round(FirewallPassWeight/FirewallWeight * 100))
                  myContext.setDataReadinessPercentBar(Math.round(DataPassWeight/DataWeight * 100))
                  myContext.setPrivacyReadinessPercentBar(Math.round(PrivacyPassWeight/PrivacyWeight * 100))
                                         
                  


                  DeviceInfo.getDeviceName().then(deviceName => {  
                                            myContext.setRunStatus("Uploading test results to server...");
                  
                          setRunProg((state) => {
                            console.log(state); 
                            return state+10;
                          });
                          

                    fetch("http://54.177.45.25/mobileapi/fetchexecid/", { method: 'GET', redirect: 'follow', })
                      .then(FetchExecIdResponse => FetchExecIdResponse.json())
                      .then(FetchExecIdResult => {
                        console.log(FetchExecIdResult.exec_id);

                        let formdata = new FormData();
                        formdata.append("user_id", myContext.UserIDValue);
                        formdata.append("exec_id", FetchExecIdResult.exec_id);
                        formdata.append("device_id", DeviceInfo.getUniqueId());
                        formdata.append("overall_score", Math.round(passedWeight/totalWeight * 100));
                        formdata.append("device_name", deviceName);
                        fetch("http://54.177.45.25/mobileapi/savescanresult/", { method: 'POST', body: formdata, })
                          .then(SaveScanResultResponse => SaveScanResultResponse.json())
                          .then(SaveScanResultResult => {
                            console.log(SaveScanResultResult)
                            setRunProg((state) => {
                              console.log(state); 
                              return state+10;
                              
                            });
                            
                            myContext.setRunStatus("Security Check Complete!");
                            myContext.setTimeStampOfLastScan("based on last scan on " +D.toDateString());
                            setRunCloseButtonColor("#00ACEB")
                            setRunCloseButton(false)
                          })
                          .catch(SaveScanResultError => console.log('SaveScanResultError', SaveScanResultError));
                      })
                      .catch(FetchExecIdError => console.log('FetchExecIdError', FetchExecIdError));

                  });

                  




    
    console.log("done")



                  //< -- setting state 

                          




    });

  
    console.log("xyz")

  

  }

  
  function getWifiValue() {
    setWifiCloseButton(true)

    myContext.setNavigationVar(navigation);
                  myContext.setWifiStatus("Checking WiFi...");
                      setWifiProg((state) => {
                    console.log(state); 
                    return state+50;
                  });

    //console.log('wifi called')
    TestModule.getWifi((info) => {
                  

      //console.log(info)
      //console.log("x" == "x" ? 'tru' : 'fal')
      myContext.setWifiTestInfo(JSON.parse(info));
      myContext.setTotalWifiTests(JSON.parse(info).wifiCheck.length)
      myContext.setSSID(JSON.parse(info).ssid)
      
      //console.log(Object.values(JSON.parse(info))[1])
      
      let i;
      let passVal = 0;
      let failVal = 0;

      for (i = 0; i < JSON.parse(info).wifiCheck.length; i++) {
             
       

        if (JSON.parse(info).wifiCheck[i].value == "PASS") {
          passVal++;
        }
        else {
          failVal++;
        }
      }
      myContext.setPassedWifiTests(passVal);
      myContext.setFailedWifiTests(failVal);

                  setWifiProg((state) => {
                    console.log(state); 
                    return state+30;
                  });


      //was findWifiScore() in wifimodal.js -->

      let totalWifiWeight = 0;
    let passedWifiWeight = 0;
    let failedWifiWeight = 0;

    let NumCritWifiTests = 0;
    let NumHighWifiTests = 0;
    let NumMedWifiTests = 0;
    let NumLowWifiTests = 0;

    let PasswordWifiPassWeight = 0;
    let EncryptionWifiPassWeight = 0;
    let FirewallWifiPassWeight = 0;
    let DnsWifiPassWeight = 0;
    let EavesdroppingWifiPassWeight = 0;

    let PasswordWifiWeight = 0;
    let EncryptionWifiWeight = 0;
    let FirewallWifiWeight = 0;
    let DnsWifiWeight = 0;
    let EavesdroppingWifiWeight = 0;

    //console.log(tempJSON);
    
     
    for (i = 0; i <JSON.parse(info).wifiCheck.length; i++) {
           
       

      totalWifiWeight += JSON.parse(info).wifiCheck[i].weight;
      if (JSON.parse(info).wifiCheck[i].value == "PASS") {
        passedWifiWeight += JSON.parse(info).wifiCheck[i].weight;
      }
      else{
        failedWifiWeight += JSON.parse(info).wifiCheck[i].weight;

        if (JSON.parse(info).wifiCheck[i].weight >= 9)
          NumCritWifiTests++;
        else if (JSON.parse(info).wifiCheck[i].weight >= 6)
          NumHighWifiTests++;
        else if (JSON.parse(info).wifiCheck[i].weight >= 4)
          NumMedWifiTests++;
        else
          NumLowWifiTests++;
      }
    }


    //wifi table ->>

    for ( i = 0; i < JSON.parse(info).wifiCheck.length ; i++) {
     
       

      let tempName = JSON.parse(info).wifiCheck[i].name;
      let tempStatus = JSON.parse(info).wifiCheck[i].value;
      let tempWeight = JSON.parse(info).wifiCheck[i].weight;

      if ( tempStatus == 'PASS') {
        
        if ( tempName == 'isHiddenSSID') {
          PasswordWifiWeight += tempWeight;
          PasswordWifiPassWeight += tempWeight;
        }

        if ( tempName == 'isWifiEncrypted') {
          EncryptionWifiWeight += tempWeight;
          EncryptionWifiPassWeight += tempWeight;
        }

        if ( tempName == 'isport1Available' || tempName == 'isport2Available' || tempName == 'isport3Available' || tempName == 'isport4Available' ) {
          FirewallWifiWeight += tempWeight;
          FirewallWifiPassWeight += tempWeight;
        }

        if ( tempName == 'dnsPos') {
          DnsWifiWeight += tempWeight;
          DnsWifiPassWeight += tempWeight;
        }

        if ( tempName == 'isIpDefaultRouter' || tempName == 'isRouterAdminAccess' || tempName == 'isVpnNat') {
          EavesdroppingWifiWeight += tempWeight;
          EavesdroppingWifiPassWeight += tempWeight;
        }

      }
      else{
        if ( tempName == 'isHiddenSSID') {
          PasswordWifiWeight += tempWeight;
        }

        if ( tempName == 'isWifiEncrypted') {
          EncryptionWifiWeight += tempWeight;
        }

        if ( tempName == 'isport1Available' || tempName == 'isport2Available' || tempName == 'isport3Available' || tempName == 'isport4Available' ) {
          FirewallWifiWeight += tempWeight;
        }

        if ( tempName == 'dnsPos') {
          DnsWifiWeight += tempWeight;
        }

        if ( tempName == 'isIpDefaultRouter' || tempName == 'isRouterAdminAccess' || tempName == 'isVpnNat') {
          EavesdroppingWifiWeight += tempWeight;
        }

      }
      
    }
                  

    myContext.setPasswordWifiPercentBar(Math.round(PasswordWifiPassWeight/PasswordWifiWeight * 100))
    myContext.setEncryptionWifiPercentBar(Math.round(EncryptionWifiPassWeight/EncryptionWifiWeight * 100))
    myContext.setFirewallWifiPercentBar(Math.round(FirewallWifiPassWeight/FirewallWifiWeight * 100))
    myContext.setDnsWifiPercentBar(Math.round(DnsWifiPassWeight/DnsWifiWeight * 100))
    myContext.setEavesdroppingWifiPercentBar(Math.round(EavesdroppingWifiPassWeight/EavesdroppingWifiWeight * 100))

    //<--wifi table 


    //wifi number of failed tests critical, high, med, low:
    myContext.setCritWifiTests(NumCritWifiTests);
    myContext.setHighWifiTests(NumHighWifiTests);
    myContext.setMedWifiTests(NumMedWifiTests);
    myContext.setLowWifiTests(NumLowWifiTests);
    //
        myContext.setTotalWifiWeight(totalWifiWeight);
    myContext.setFailedWifiWeight(failedWifiWeight);
    myContext.setReadinessWifiPercent(Math.round(passedWifiWeight/totalWifiWeight * 100))

    if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 67) {
                      myContext.setReadinessWifiTitle("Low");
                      myContext.setReadinessWifiColor("#2E7D32");
                    }
                else if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 33) {
                      myContext.setReadinessWifiTitle("Medium");
                      myContext.setReadinessWifiColor("#F57F17");
                    }
                else if (Math.round(passedWifiWeight/totalWifiWeight * 100) >= 15) {
                      myContext.setReadinessWifiTitle("High");
                      myContext.setReadinessWifiColor("#E65100");
                    }
                else {
                      myContext.setReadinessWifiTitle("Critical") ;
                      myContext.setReadinessWifiColor("#B71C1C");
                    }


                  setWifiProg((state) => {
                    console.log(state); 
                    return state+20;
                  });
                  myContext.setWifiStatus("WiFi Checks Complete!")
                  setWifiCloseButtonColor("#00ACEB")
                  setWifiCloseButton(false)

      // <-- was findWifiScore() in wifimodal.js 
    console.log("getwifi done")
    });
    


  }
  

  function fetchDevices() {
    setSyncCloseButton(true)

    myContext.setNavigationVar(navigation);
                  myContext.setSyncStatus("Syncing Devices...");
                          setSyncProg((state) => {
                            console.log(state); 
                            return state+30;
                          });
    var formdata = new FormData();
    let tempName, tempRisk, tempReadiness;
    let tempDeviceLoop = [];
    var requestOptions = {
      method: 'GET',
      //body: formdata,
      redirect: 'follow'
    };
    console.log(myContext.UserIDValue)
    //let FetchURL = "http://54.177.45.25/mobileapi/deviceinfo/?user_id="+{myContext.UserIDValue}.toString();
    fetch("http://54.177.45.25/mobileapi/deviceinfo/?user_id="+myContext.UserIDValue, requestOptions)
    .then(response => response.json())
    .then(result => {
                          setSyncProg((state) => {
                            console.log(state); 
                            return state+30;
                          });      
      myContext.setDeviceInfo(result)
      let tempName, tempRisk, tempReadiness;

    let i;
    let tempDeviceLoop = [];

    for(i = 1; i <= Object.keys(result[0].verified_devices).length; i++) {
      tempName = result[0].verified_devices[i].name;
      tempRisk = result[0].verified_devices[i].risk;
      tempReadiness = parseInt(result[0].verified_devices[i].security_readiness)

      tempDeviceLoop.push(
        <View key = {i} style= {{ alignItems: 'center', justifyContent: 'center', flex: 1, flexDirection: 'row', width: '100%', height: '100%', paddingTop: hp(2.5), paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

          <View style = {{ flex: 1.5, }}>
            <Text style={{flex: 1, color: 'white', fontSize: wp(4)}} numberOfLines={1}>{tempName}</Text>
          </View>

          <View style = {{ flex: 2, paddingRight: wp(2), paddingLeft: wp(1)}}>
            <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
              <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: tempReadiness > 15 ? tempReadiness.toString()+'%' : '15%', backgroundColor: tempReadiness > 67 ? '#2E7D32' : tempReadiness > 33 ? '#F57F17' : tempReadiness > 15 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                <Text style = {{color: 'white', fontSize: wp(2.5)}}>{tempReadiness}%</Text>
              </View>
            </View>
          </View>

          <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: tempReadiness > 67 ? '#2E7D32' : tempReadiness > 33 ? '#F57F17' : tempReadiness > 15 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                <Text style = {{color: 'white', fontSize: wp(3.5)}}>{tempRisk == "Medium" ? "Med" : (tempRisk == "Critical" ? "Crit" : tempRisk)}</Text>
              </View>
          </View>

        </View>

      )
      
    }

    myContext.setDeviceLoop(tempDeviceLoop);
                          setSyncProg((state) => {
                            console.log(state); 
                            return state+40;
                          });    
                          myContext.setSyncStatus("Sync Complete!")
                          setSyncCloseButton(false)
                          setSyncCloseButtonColor("#00ACEB")
      


    })
    .catch(error => console.log('error', error));

    //myContext.setDeviceLoop(tempDeviceLoop);

  }

  
  function RunModalClosed() {
    console.log("run modal closed")
    setRunCloseButtonColor('gray')
    myContext.setRunStatus("Running Security Checks...");

  }
  function WifiModalClosed() {
    setWifiCloseButtonColor('gray')
    myContext.setWifiStatus("Checking WiFi...");
  }
  function SyncModalClosed() {
    setSyncCloseButtonColor('gray')
    myContext.setSyncStatus("Syncing Devices...");
  }

    

  return (
    
      <View style = {{flex: 1,}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={RunModalVisibleValue}
          onShow={getTestValue}
          //onDismiss={RunModalClosed}


          
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              
              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A' }} >
                <Text style={styles.modalText}>Security Check</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 4, backgroundColor: '#1C2B3A' }} >
                {/*<Progress.Bar
                                  style={styles.progressBar}
                                  progress={runValue}
                                />*/}
                {/*<Text style= {{ color: '#D3D3D3'}}>{Math.round(myContext.RunPercentValue * 100)}%</Text>*/}
                <Text style= {{ color: '#D3D3D3'}}>{RunProg}%</Text>
                {/*<Text style= {{ color: '#D3D3D3'}}>{myContext.globalVarValue}</Text>*/}
                <Text style = {{ marginTop: hp(5), fontSize: wp(4), color: '#D3D3D3' }}>{myContext.RunStatusValue}</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A',  }} >
                <TouchableHighlight
                  style={ { ...styles.openButton, backgroundColor: RunCloseButtonColor, }}
                  onPress={() => {
                    //findGeneralScore();
                    setRunModalVisible(false);
                    setRunProg(0)
                    setRunCloseButtonColor('gray')
                    myContext.setRunStatus("Running Security Checks...");
                    //myContext.NavigationVarValue.navigate("Status");

                  }}
                  disabled = {RunCloseButton}
                >
                    <View style = {{ ...styles.centrify, flex: 1, backgroundColor: RunCloseButtonColor,  }} >
                  <Text style={styles.textStyle}>     Close     </Text>
                  </View>
                </TouchableHighlight>                
              </View>
            </View>
          </View>
        </Modal>




        <Modal
          animationType="slide"
          transparent={true}
          visible={SyncModalVisibleValue}
          onShow={fetchDevices}
          //onDismiss={SyncModalClosed}


          
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              
              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A' }} >
                <Text style={styles.modalText}>Device Sync</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 4, backgroundColor: '#1C2B3A' }} >
                {/*<Progress.Bar
                                  style={styles.progressBar}
                                  progress={syncValue}
                                />*/}
                <Text style= {{ color: '#D3D3D3'}}>{SyncProg}%</Text>
                {/*<Text style= {{ color: '#D3D3D3'}}>{myContext.globalVarValue}</Text>*/}
                <Text style = {{ marginTop: hp(5), fontSize: wp(4), color: '#D3D3D3' }}>{myContext.SyncStatusValue}</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A',  }} >
                <TouchableHighlight
                  style={ { ...styles.openButton, backgroundColor: SyncCloseButtonColor, }}
                  onPress={() => {
                    //setDeviceSync();
                    setSyncModalVisible(false);
                    setSyncProg(0);
                    setSyncCloseButtonColor('gray')
                    myContext.setSyncStatus("Syncing Devices...");
                    myContext.NavigationVarValue.navigate("Devices");

                  }}
                  disabled = {SyncCloseButton}
                >
                  <View style = {{ ...styles.centrify, flex: 1, backgroundColor: SyncCloseButtonColor,  }} >
                  <Text style={styles.textStyle}>     Close     </Text>
                  </View>
                </TouchableHighlight>                
              </View>
            </View>
          </View>
        </Modal>


        <Modal
          animationType="slide"
          transparent={true}
          visible={WifiModalVisibleValue}
          onShow={getWifiValue}
          //onDismiss={WifiModalClosed}

          
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              
              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A' }} >
                <Text style={styles.modalText}>Wifi Check</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 4, backgroundColor: '#1C2B3A' }} >
                {/*<Progress.Bar
                                  style={styles.progressBar}
                                  progress={wifiValue}
                                />*/}
                <Text style= {{ color: '#D3D3D3'}}>{WifiProg}%</Text>
                {/*<Text style= {{ color: '#D3D3D3'}}>{myContext.globalVarValue}</Text>*/}
                <Text style = {{ marginTop: hp(5), fontSize: wp(4), color: '#D3D3D3' }}>{myContext.WifiStatusValue}</Text>
              </View>

              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: '#1C2B3A',  }} >
                <TouchableHighlight
                  style={ { ...styles.openButton, backgroundColor: WifiCloseButtonColor, }}
                  onPress={() => {
                    //findWifiScore();
                    setWifiModalVisible(false);
                    setWifiProg(0)
                    setWifiCloseButtonColor('gray')
                    myContext.setWifiStatus("Checking WiFi...");
                    
                    //console.log(myContext.NavigationVarValue);
                    myContext.NavigationVarValue.navigate("WiFi");

                  }}
                  disabled = {WifiCloseButton}
                >
              <View style = {{ ...styles.centrify, flex: 1, backgroundColor: WifiCloseButtonColor,  }} >
                  <Text style={styles.textStyle}>     Close     </Text>
                  </View>
                </TouchableHighlight>                
              </View>
            </View>
          </View>
        </Modal>



        <View style = {{flex: 10,}}>
          <Tab.Navigator
        
        tabBarOptions=
        { (PixelRatio.get() <= 1.5) ?
              {
            activeTintColor: 'white',
            inactiveTintColor: '#00ACEB',
            tabStyle: {   
              borderColor: bordCol, 
              borderWidth: bordWid,  
              //flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            },
            style: { 
              backgroundColor: '#18222E' , 
              borderColor: 'white', 
              borderWidth: 0, 
              paddingTop: hp(1),
              //justifyContent: 'center',
              //alignItems: 'center',
              borderWidth: bordWid,
              borderColor: bordCol,
            },

            indicatorStyle: {
              backgroundColor: '#18222E',
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: bordWid,
              borderColor: bordCol,

            },
            labelStyle: {
              flex: 0.5, 
              width: "100%", 
              borderWidth: 0, 
              borderColor: 'white', 
              fontSize: wp(2),
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: bordWid,
              borderColor: bordCol,
            },
            iconStyle: {
              flex: 0.75,
              width: "100%",
              borderWidth: 0,
              borderColor: 'white',
              marginBottom:hp(1),
              marginTop: hp(3),
              justifyContent: 'center',
              alignItems: 'center',
              borderWidth: bordWid,
              borderColor: bordCol,
            },
            useNativeDriver: true,
            showIcon: true,
          }
             : 
             {
              activeTintColor: 'white',
              inactiveTintColor: '#00ACEB',
              //tabStyle: { width: 100 },
              style: { 
                backgroundColor: '#18222E' 
              },
              indicatorStyle: {
                backgroundColor: '#18222E',
              },
              useNativeDriver: true,
              showIcon: true,
            }
            
        }
      >
        <Tab.Screen 
          name="Status" 
          component={Home} 
          options = {{ 
            tabBarIcon: ({focused}) => {
              if (focused == true){
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/home_menu_activexxxhdpi.png')}
              />
                )
              }
              else {
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/home_menu_normalxxxhdpi.png')}
              />
                )
              }
            }
              
          }}
        />
        <Tab.Screen 
          name="Details" 
          component={Details}
          options = {{ 
            tabBarIcon: ({focused}) => {
              if (focused == true){
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/details_menu_activexxxhdpi.png')}
              />
                )
              }
              else {
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/details_menu_normalxxxhdpi.png')}
              />
                )
              }
            }
          }} 
        />
        <Tab.Screen 
          name="Remediate" 
          component={Remediation} 
          options = {{ 
            tabBarIcon: ({focused}) => {
              if (focused == true){
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/remediation_menu_menu_activexxxhdpi.png')}
              />
                )
              }
              else {
                return(
                  <Image
                style={{ width: wp(6), height: hp(4),  }}
                source={require('../assets/android/4x/remediation_menu_normalxxxhdpi.png')}
              />
                )
              }
            }
          }}
        />
        
        
      </Tab.Navigator>
        </View>
        <View style = {{flex: 1,}}>
           <View style={{ ...styles.centrify, flex: 1, flexDirection: 'row', backgroundColor: '#1C2B3A'}}>
          <View style={{ ...styles.centrify, flex: 1, }}>
            <View style={{ ...styles.centrify, flex: 4, alignItems: 'flex-end', paddingRight: wp(2)}}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                            setRunModalVisible(!RunModalVisibleValue)

                  //console.log("buttonpress")
                  // myContext.setNavigationVar(navigation);
                  // myContext.setRunStatus("Running General Checks...");
                  //resetRun();
                  //animateRun();
                      //getTestValue();

                  

                }}
                //onPress={globalFunc}
              >
                <Image
                style={{ width: wp(6), height: wp(6), }}
                source={require('../assets/android/4x/run_btm_bar_menu_normalxxxhdpi.png')}
              />
              <Text style = {{   color: '#FFFFFF',  }}>Test All</Text>
              </TouchableOpacity>
              
            </View>
             
          </View>

          <View style={{ ...styles.centrify, flex: 1, }}>
            <View style={{ ...styles.centrify, flex: 4, }}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  setWifiModalVisible(true);
                  
                  
                  //getWifiValue();
                  //animateWifi();
                }}
              >
                <Image
                style={{ width: wp(6), height: wp(6), }}
                source={require('../assets/android/4x/wifi_check_btm_bar_menu_normalxxxhdpi.png')}
              />
                <Text style = {{   color: '#FFFFFF',  }}>Test WiFi</Text>
              </TouchableOpacity>
            </View>
            
          </View>

          <View style={{ ...styles.centrify, flex: 1, }}>
            <View style={{ ...styles.centrify, flex: 4, alignItems: 'flex-start', paddingLeft: wp(2)}}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  setSyncModalVisible(true);
                  
                  
                  //animateSync();
                      //fetchDevices();


                }}
              >
                <Image
                style={{ width: wp(6), height: wp(6), }}
                source={require('../assets/android/4x/sync_btm_bar_menu_normalxxxhdpi.png')}
              />
                <Text style = {{   color: '#FFFFFF',  }}>Sync</Text>
              </TouchableOpacity>
            </View>
             
          </View>

          
          
        </View>
        </View>

      </View>
    
  );
}

export default function Testboard({ navigation }) {
  return (

    <Stack.Navigator>

      <Stack.Screen name="Home" component={MyTabs} options={{
          
          headerTitleStyle: {
            fontSize: windowWidth/10,

            color: 'red',
          },
          headerStyle: {
            backgroundColor: '#1C2B3A',
            height: wp(15),
          },
          title: HeaderComponent(),
          headerLeft: () => (
            <TouchableWithoutFeedback  onPress={() => {navigation.toggleDrawer()
              console.log(PixelRatio.get())
              console.log(DeviceInfo.getUniqueId())
              

         }}>
              <View style = {{ paddingLeft: windowWidth/30, }}>
                <Image
                  style={{ width: 35, height: 35, }}
                  source={require('../assets/android/4x/top_bar_menu_iconxxxhdpi.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            
          ),
        }}/>
      
    </Stack.Navigator>
  );
}



const styles = StyleSheet.create({

  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(100),
    
    borderWidth: 0,
  },
  centrify:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  button: {
    alignItems: "center",
    
    
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(4),
  },
  modalView: {
    height: hp(50),

    width: wp(80),
    margin: hp(3),
    backgroundColor: "#1C2B3A",
    borderRadius: 20,
    padding: wp(5),
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: wp(3),
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    
    
         
  },
  modalText: {
    marginTop: hp(5),
    marginBottom: hp(3),
    textAlign: "center",
    fontSize: wp(5),
    color: '#D3D3D3'
         
  },

  progressBarContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    //paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: wp(3),
  },
  progressBar: {
    margin: wp(3),
  },
});

