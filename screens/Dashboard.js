import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { PixelRatio, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
import { VictoryLegend, VictoryPie, VictoryLabel, VictoryContainer } from "victory-native";
//import Constants from 'expo-constants';
//import {} from 'react';
//import { useFonts } from '@use-expo/font';
//import * as Progress from 'react-native-progress';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppContext from "../components/AppContext";
//import {styles} from '../styles/DashboardStyle';
import { Dimensions } from 'react-native';
//import ModalTest from '../components/modals.js';

import RunModal from '../components/RunModal.js';
import SyncModal from '../components/SyncModal.js';
import WifiModal from '../components/WifiModal.js';

import globalFunc from '../components/global.js';

import {NativeModules} from 'react-native';

import DeviceInfo from 'react-native-device-info';




var TestModule = NativeModules.TestModule;


const windowWidth = Dimensions.get('screen').width;
const windowHeight = Dimensions.get('screen').height;
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();




const colorOne = "#b0ceff";
const colorTwo = "#c6dbff";
const colorThree = "#d7e6ff";
const colorFive = "#e7f0ff";
const colorFour = "white";
const colorSix = "#add8e6";
const colorSeven = "#add8e6";

const readiness = 50;
// const spreadData = [{ x: "35%", y: 35 }, { x: "20%", y: 20 }, { x: "5%", y: 5 }, { x: "25%", y: 25 }, { x: "15%", y: 15 }, { x: "30%", y: 30 },];
const spreadColor = [colorOne, colorTwo, colorThree, colorFour, colorFive, colorSix, colorSeven];
const readinessData = [{ y: readiness }, { y: 100 - readiness }, ];
const readinessColorMiddle = ["black", "black"];
const readinessColorOuter = ["white", "white"];
const labelData = [''];





function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}



export default function DashboardScreen({navigation}) {

  const myContext = useContext(AppContext);
  const [ctr, setCtr] = useState(0);


  


  const [buttonOnePressed, setButtonOnePressed] = useState(true);
  const [buttonTwoPressed, setButtonTwoPressed] = useState(false);
  const [buttonThreePressed, setButtonThreePressed] = useState(false);
  const [buttonFourPressed, setButtonFourPressed] = useState(false);
  const [buttonFivePressed, setButtonFivePressed] = useState(false);
  const [buttonSixPressed, setButtonSixPressed] = useState(false);
  const [buttonSevenPressed, setButtonSevenPressed] = useState(false);

  
  const [activeButtonLabel, setActiveButtonLabel] = useState('Device');
  
  const [buttonOneStyle, setButtonOneStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#7BB7FF', borderRadius: 15, backgroundColor: '#7BB7FF'});
  const [buttonOneTextStyle, setButtonOneTextStyle] = useState({color: '#000000', height: hp(3), color: 'black'});

  const [buttonTwoStyle, setButtonTwoStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#bee0ff', borderRadius: 15,});
  const [buttonTwoTextStyle, setButtonTwoTextStyle] = useState({color: '#bee0ff', height: hp(3), color: 'white'});

  const [buttonThreeStyle, setButtonThreeStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#d6ebff', borderRadius: 15,});
  const [buttonThreeTextStyle, setButtonThreeTextStyle] = useState({color: '#d6ebff', height: hp(3), color: 'white'});

  const [buttonFourStyle, setButtonFourStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#f2f9ff', borderRadius: 15,});
  const [buttonFourTextStyle, setButtonFourTextStyle] = useState({color: '#f2f9ff', height: hp(3), color: 'white'});

  const [buttonFiveStyle, setButtonFiveStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
  const [buttonFiveTextStyle, setButtonFiveTextStyle] = useState({color: '#fffffF', height: hp(3), color: 'white'});

  const [buttonSixStyle, setButtonSixStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
  const [buttonSixTextStyle, setButtonSixTextStyle] = useState({color: 'white', height: hp(3), color: 'white'});

  const [buttonSevenStyle, setButtonSevenStyle] = useState({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
  const [buttonSevenTextStyle, setButtonSevenTextStyle] = useState({color: 'white', height: hp(3), color: 'white'});

  let spreadData = [
    { x: "Device", y: myContext.DeviceReadinessPercentValue }, 
    { x: "Email", y: myContext.EmailReadinessPercentValue }, 
    { x: "WiFi", y: myContext.WifiReadinessPercentValue }, 
    { x: "Data", y: myContext.DataReadinessPercentValue }, 
    { x: "Firewall", y: myContext.FirewallReadinessPercentValue }, 
    { x: "Web", y: myContext.WebReadinessPercentValue },
    { x: "Privacy", y: myContext.PrivacyReadinessPercentValue },
  ];


  function resetButtons() {
    //console.log("===================")
    setButtonOnePressed(false);
    setButtonTwoPressed(false);
    setButtonThreePressed(false);
    setButtonFourPressed(false);
    setButtonFivePressed(false);
    setButtonSixPressed(false);
    setButtonSevenPressed(false);

    setButtonOneStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#9fcFfb', borderRadius: 15,});
    setButtonOneTextStyle({color: '#9fcFfb', height:hp(3), color: 'white'});
    setButtonTwoStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#bee0ff', borderRadius: 15,});
    setButtonTwoTextStyle({color: '#bee0ff', height: hp(3), color: 'white'});
    setButtonThreeStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#d6ebff', borderRadius: 15,});
    setButtonThreeTextStyle({color: '#d6ebff', height:hp(3), color: 'white'});
    setButtonFourStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#f2f9ff', borderRadius: 15,});
    setButtonFourTextStyle({color: '#f2f9ff', height:hp(3), color: 'white'});
    setButtonFiveStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
    setButtonFiveTextStyle({color: '#fffffF', height:hp(3), color: 'white'});
    setButtonSixStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
    setButtonSixTextStyle({color: 'white', height:hp(3), color: 'white'});
    setButtonSevenStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: '#fffffF', borderRadius: 15,});
    setButtonSevenTextStyle({color: 'white', height:hp(3), color: 'white'});

  }
  function buttonOneOnPress () {
    resetButtons();
    setButtonOnePressed(true);
    myContext.setActiveButtonValue(myContext.DeviceReadinessPercentValue);
    setActiveButtonLabel('Device');
    setButtonOneStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorOne, borderRadius: 15, backgroundColor: colorOne});
    setButtonOneTextStyle({color: '#000000', height:hp(3),});
    if (myContext.DeviceReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.DeviceReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.DeviceReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonTwoOnPress () {
    resetButtons();
    setButtonTwoPressed(true);
    myContext.setActiveButtonValue(myContext.EmailReadinessPercentValue);
    setActiveButtonLabel('Email');
    setButtonTwoStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorTwo, borderRadius: 15, backgroundColor: colorTwo});
    setButtonTwoTextStyle({color: '#000000', height: hp(3),});
    if (myContext.EmailReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.EmailReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.EmailReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonThreeOnPress () {
    resetButtons();
    setButtonThreePressed(true);
    myContext.setActiveButtonValue(myContext.WifiReadinessPercentValue);
    setActiveButtonLabel('Wifi');
    setButtonThreeStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorThree, borderRadius: 15, backgroundColor: colorThree});
    setButtonThreeTextStyle({color: '#000000', height:hp(3),});
    if (myContext.WifiReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.WifiReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.WifiReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonFourOnPress () {
    resetButtons();
    setButtonFourPressed(true);
    myContext.setActiveButtonValue(myContext.DataReadinessPercentValue);
    setActiveButtonLabel('Data');
    setButtonFourStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorFour, borderRadius: 15, backgroundColor: colorFour});
    setButtonFourTextStyle({color: '#000000', height:hp(3),});
    if (myContext.DataReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.DataReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.DataReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonFiveOnPress () {
    resetButtons();
    setButtonFivePressed(true);
    myContext.setActiveButtonValue(myContext.FirewallReadinessPercentValue);
    setActiveButtonLabel('Firewall');
    setButtonFiveStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorFive, borderRadius: 15, backgroundColor: colorFive});
    setButtonFiveTextStyle({color: '#000000', height:hp(3),});
    if (myContext.FirewallReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.FirewallReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.FirewallReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonSixOnPress () {
    resetButtons();
    setButtonSixPressed(true);
    myContext.setActiveButtonValue(myContext.WebReadinessPercentValue);
    setActiveButtonLabel('Web');
    setButtonSixStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorSix, borderRadius: 15, backgroundColor: colorSix});
    setButtonSixTextStyle({color: '#000000', height:hp(3),});
    if (myContext.WebReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.WebReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.WebReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  function buttonSevenOnPress () {
    resetButtons();
    setButtonSevenPressed(true);
    myContext.setActiveButtonValue(myContext.PrivacyReadinessPercentValue);
    setActiveButtonLabel('Privacy');
    setButtonSevenStyle({...styles.centrify, flex:1, borderWidth: 2, borderColor: colorSeven, borderRadius: 15, backgroundColor: colorSeven});
    setButtonSevenTextStyle({color: '#000000', height:hp(3),});
    if (myContext.PrivacyReadinessPercentBarValue > 67) {
      myContext.setActiveButtonRiskValue("Low")
      myContext.setActiveButtonRiskValueColor("#2E7D32")
    }
    else if (myContext.PrivacyReadinessPercentBarValue > 33) {
      myContext.setActiveButtonRiskValue("Medium")
      myContext.setActiveButtonRiskValueColor("#F57F17")
    }
    else if (myContext.PrivacyReadinessPercentBarValue > 15) {
      myContext.setActiveButtonRiskValue("High")
      myContext.setActiveButtonRiskValueColor("#E65100")
    }
    else {
      myContext.setActiveButtonRiskValue("Critical")
      myContext.setActiveButtonRiskValueColor("#B71C1C")
    }
  }
  


  
  return (

    <View style={{ ...styles.centrify, flex: 1 }}>
      
      



        <View style={{ ...styles.centrify, flex: 1, backgroundColor: '#18222E'}}>
        
          {/*<SafeAreaView>
            <ScrollView>*/}
              <View style={{ ...styles.centrify, flex: 1 }}>
                <View style = {{ ...styles.container, flex: 1, }}>
                  <View style={{ paddingTop: hp(1), }}>
                    <Text style= {{ fontSize: wp(6),   color: '#D3D3D3',   }}>Overall Security</Text>
                    

                  </View>
                </View>
                <View style = {{ ...styles.container, flexDirection: 'row', flex: 4, }}>
                  <View style = {{ ...styles.container, flex: 2  }}>
                    <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0,}}>
                      <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0,}}>
                      <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(55)}
                        width={wp(55)}
                      >
                        
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(14.5)}
                          colorScale={readinessColorOuter}
                          innerRadius={wp(17.5)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(15.9)}
                          colorScale={readinessColorMiddle}
                          innerRadius={wp(16.1)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(15.25)}
                          colorScale={[myContext.ReadinessGeneralColorValue, "transparent"]}
                          innerRadius={wp(16.75)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />

                        
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(6), fill: 'white'}}
                          x={wp(27.5)}
                          y={wp(25)}
                          
                          text={[myContext.ReadinessGeneralPercentValue+"%"]}
                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3.5), fill: 'white'}}
                          x={wp(22.5)}
                          y={wp(30)}
                          
                          text={["Risk: "]}
                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3.5), fill: myContext.ReadinessGeneralColorValue}}
                          x={wp(32.5)}
                          y={wp(30)}
                          
                          text={[myContext.ReadinessGeneralTitleValue]}
                        />
                      </VictoryContainer>
                      
                    </View>
                    {/*<View style = {{ ...styles.centrify, flex: 1, paddingBottom: 0, paddingTop: 0, borderWidth: 1 }}>
                      <Text style = {{ fontSize: 15,      }}> {readiness}% </Text>
                      <Text style = {{ fontSize: 15,      }}> {securityLevel} </Text>
                    </View>*/}
                    </View>
                    
                  </View>
                  <View style = {{ ...styles.container, flex: 1, paddingTop: hp(0), flexDirection: 'column', marginRight: windowWidth/50, }}>
                    <View style = {{ ...styles.centrify, flex: 0.75,}}>
                      <Text style = {{color: 'white', fontSize: wp(2.5)}}>Failed tests: {(myContext.TotalGeneralTestsValue == 0) ? 0 : (myContext.FailedGeneralTestsValue + myContext.FailedWifiTestsValue)} out of {(myContext.TotalGeneralTestsValue == 0 )? 0 : (myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue)}</Text>
                    </View>
                    <View style = {{ ...styles.centrify, flex: 1, flexDirection: 'row',}}>
                    <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.CritGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue == 0 ? 1 : myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue) - myContext.CritGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#B71C1C','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.CritGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Critical </Text>
                      </View>

                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.HighGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue == 0 ? 1 : myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue) - myContext.HighGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#E65100','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.HighGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> High </Text>
                      </View>

                    </View>
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, flexDirection: 'row',}}>
                    <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.MedGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue== 0 ? 1 : myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue) - myContext.MedGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#F57F17','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.MedGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Medium </Text>
                      </View>

                    </View>
                    <View style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.LowGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue== 0 ? 1 : myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue) - myContext.LowGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#2E7D32','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.LowGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Low </Text>
                      </View>

                    </View>
                    </View>
                  </View>
                </View>

                <View style = {{ ...styles.container, flex: 1, }}>
                  <View style={{  }}>
                    <Text style= {{ fontSize: wp(6),   color: '#D3D3D3'   }}>Threat Breakdown</Text>
                  </View>
                </View>
                <View style = {{ ...styles.container, flexDirection: 'row', flex: 4, }}>
                  <View style = {{ ...styles.container, flex: 1, alignItems: 'flex-end',  }}>
                    <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(40)}
                        width={wp(40)}
                      >
                            <VictoryPie

                              animate={{ easing: 'exp',}}
                              padAngle={3}
                              radius={
                                ({ datum }) => {
                                  // console.log(buttonOnePressed+ " "+ datum.y + " "+ myContext.DeviceReadinessPercentValue);
                                  // console.log(buttonTwoPressed+ " "+ datum.y + " "+ myContext.EmailReadinessPercentValue);
                                  // console.log(buttonThreePressed+ " "+ datum.y + " "+ myContext.WifiReadinessPercentValue);
                                  // console.log(buttonFourPressed+ " "+ datum.y + " "+ myContext.DataReadinessPercentValue);
                                  // console.log(buttonFivePressed+ " "+ datum.y + " "+ myContext.FirewallReadinessPercentValue);
                                  // console.log(" ")
                                  

                                  if (buttonOnePressed == true && datum.x == "Device") {
                                    return wp(16) 
                                  }
                                  else if (buttonTwoPressed == true && datum.x == "Email") {
                                    return wp(16) 
                                  }
                                  else if (buttonThreePressed == true && datum.x == "WiFi") {
                                    return wp(16)
                                  }
                                  else if (buttonFourPressed == true && datum.x == "Data") {
                                    return wp(16) 
                                  }
                                  else if (buttonFivePressed == true && datum.x == "Firewall") {
                                    return wp(16) 
                                  }
                                  else if (buttonSixPressed == true && datum.x == "Web") {
                                    return wp(16) 
                                  }
                                  else if (buttonSevenPressed == true && datum.x == "Privacy") {
                                    return wp(16) 
                                  }
                                  else {
                                    return wp(14.5)
                                  }
                                  
                                }
                              }
                              data={spreadData}
                              width={wp(40)}
                              height={wp(40)}
                              colorScale={spreadColor}
                              innerRadius={
                                ({ datum }) => {
                                  // console.log(buttonOnePressed+ " "+ datum.y + " "+ myContext.DeviceReadinessPercentValue);
                                  // console.log(buttonTwoPressed+ " "+ datum.y + " "+ myContext.EmailReadinessPercentValue);
                                  // console.log(buttonThreePressed+ " "+ datum.y + " "+ myContext.WifiReadinessPercentValue);
                                  // console.log(buttonFourPressed+ " "+ datum.y + " "+ myContext.DataReadinessPercentValue);
                                  // console.log(buttonFivePressed+ " "+ datum.y + " "+ myContext.FirewallReadinessPercentValue);
                                  // console.log(" ")


                                  if (buttonOnePressed == true && datum.x == "Device") {
                                    return wp(12) 
                                  }
                                  else if (buttonTwoPressed == true && datum.x == "Email") {
                                    return wp(12) 
                                  }
                                  else if (buttonThreePressed == true && datum.x == "WiFi") {
                                    return wp(12)
                                  }
                                  else if (buttonFourPressed == true && datum.x == "Data") {
                                    return wp(12) 
                                  }
                                  else if (buttonFivePressed == true && datum.x == "Firewall") {
                                    return wp(12) 
                                  }
                                  else if (buttonSixPressed == true && datum.x == "Web") {
                                    return wp(12) 
                                  }
                                  else if (buttonSevenPressed == true && datum.x == "Privacy") {
                                    return wp(12) 
                                  }
                                  else {
                                    return wp(13.5)
                                  }
                                  
                                }
                              }
                              startAngle = {0}
                              endAngle = {360}
                              style={{
                                labels: {
                                  fontSize: 0,
                                  
                                }
                              }}
                            />{/* can add labeldata */}

                            <VictoryLabel 
                              textAnchor="middle"
                              style={{ fontSize: wp(6), fill: 'white'}}
                              x={wp(20)}
                              y={wp(20)}
                              
                              text={[myContext.ActiveButtonValue+"%"]}
                            />


                    </VictoryContainer>
                    
                  </View>
                
                  <View style = {{ ...styles.container, flex: 1, paddingTop: hp(1),}}>
                    <View style = {{ ...styles.centrify, flex: 1, }}>
                      <Text style = {{ color: 'white', fontSize: wp(3) }}>{activeButtonLabel} Risk: <Text style = {{ color: myContext.ActiveButtonRiskValueColor, fontSize: wp(3)}}>{myContext.ActiveButtonRiskValue}</Text></Text>
                    </View>
                    
                  </View>
                </View>
                <View style = {{ ...styles.container, flex: 1, paddingBottom: hp(2)}}>
                  <View style = {{ ...styles.centrify, flexDirection: 'row', height: hp(7.5)}}>
                    
                    

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonOneOnPress}>
                        <View style = {buttonOneStyle}> 
                          <Text style = {buttonOneTextStyle}> Device </Text>
                          <Text style = {buttonOneTextStyle}> {myContext.DeviceReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft:  wp(1), paddingRight:  wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonTwoOnPress}>
                        <View style = {buttonTwoStyle}> 
                          <Text style = {buttonTwoTextStyle}> Email </Text>
                          <Text style = {buttonTwoTextStyle}> {myContext.EmailReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonThreeOnPress}>
                        <View style = {buttonThreeStyle}> 
                          <Text style = {buttonThreeTextStyle}> WiFi </Text>
                          <Text style = {buttonThreeTextStyle}> {myContext.WifiReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonFourOnPress}>
                        <View style = {buttonFourStyle}> 
                          <Text style = {buttonFourTextStyle}> Data </Text>
                          <Text style = {buttonFourTextStyle}> {myContext.DataReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonFiveOnPress}>
                        <View style = {buttonFiveStyle}> 
                          <Text style = {buttonFiveTextStyle}>Firewall</Text>
                          <Text style = {buttonFiveTextStyle}> {myContext.FirewallReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonSixOnPress}>
                        <View style = {buttonSixStyle}> 
                          <Text style = {buttonSixTextStyle}> Web </Text>
                          <Text style = {buttonSixTextStyle}> {myContext.WebReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    <View style = {{ ...styles.centrify, flex: 1, paddingLeft: wp(1), paddingRight: wp(1), }}>
                      <TouchableWithoutFeedback onPress = {buttonSevenOnPress}>
                        <View style = {buttonSevenStyle}> 
                          <Text style = {buttonSevenTextStyle}>Privacy</Text>
                          <Text style = {buttonSevenTextStyle}> {myContext.PrivacyReadinessPercentValue}%</Text>
                        </View>
                      </TouchableWithoutFeedback>    
                    </View>

                    

                    






                  </View>
                </View>
              </View>
           {/* </ScrollView>
          </SafeAreaView>*/}


      
    
        </View>
       
      
      </View>
  );
}







const styles = StyleSheet.create({

  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(100),
    
    borderWidth: 0,
  },
  centrify:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  button: {
    alignItems: "center",
    
    
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(4),
  },
  modalView: {
    height: hp(50),

    width: wp(80),
    margin: hp(3),
    backgroundColor: "#1C2B3A",
    borderRadius: 20,
    padding: wp(5),
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: wp(3),
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    
    
         
  },
  modalText: {
    marginTop: hp(5),
    marginBottom: hp(3),
    textAlign: "center",
    fontSize: wp(5),
    color: '#D3D3D3'
         
  },

  progressBarContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    //paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
    padding: wp(3),
  },
  progressBar: {
    margin: wp(3),
  },
});


