import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { FlatList, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
//import { Ionicons } from '@expo/vector-icons';
import { Dimensions } from 'react-native';
//import { Table, TableWrapper, Cell, Row, Rows } from 'react-native-table-component';
//import { useFonts } from '@use-expo/font';
import { VictoryLegend, VictoryPie, VictoryLabel, VictoryContainer } from "victory-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import AppContext from "../components/AppContext";
// import RunModal from '../components/RunModal.js';
// import SyncModal from '../components/SyncModal.js';
// import WifiModal from '../components/WifiModal.js';
import {NativeModules} from 'react-native';

import { useIsFocused} from '@react-navigation/native'; 

var TestModule = NativeModules.TestModule;


const Stack = createStackNavigator();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const readiness = 50;
const readinessData = [{ y: readiness }, { y: 100 - readiness }, ];
const readinessColor = ["#db3913", "white"];
const labelData = [''];
//let bordCol = "#00ACEB";
let bordCol = "white";
let bordWid = 0;

//const readinessColorInner = [myContext.ReadinessGeneralColorValue, "transparent"];
const readinessColorMiddle = ["black", "black"];
const readinessColorOuter = ["white", "white"];





export default function DetailsScreen({navigation}) {

  //Modal stuff -->
const myContext = useContext(AppContext);
  
  
  let intervalRun, intervalSync, intervalWifi;
  
  
   

  





  // 
  useIsFocused();
  return (
    

    <View style={{ ...styles.container, flex: 1 }}>
      

      <View style = {{ ...styles.container, flex: 1, }}>
                  <View style={{ paddingTop: hp(1), }}>
                    <Text style= {{ fontSize: wp(6),   color: '#D3D3D3',   }}>Overall Security</Text>
                    

                  </View>
                </View>

      <View style = {{ ...styles.container, flexDirection: 'row', flex: 4, }}>
                  <View style = {{ ...styles.container, flex: 2  }}>
                    <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0,}}>
                      <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0,}}>
                      <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(55)}
                        width={wp(55)}
                      >
                        
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(14.5)}
                          colorScale={readinessColorOuter}
                          innerRadius={wp(17.5)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(15.9)}
                          colorScale={readinessColorMiddle}
                          innerRadius={wp(16.1)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.ReadinessGeneralPercentValue }, { y: 100 - myContext.ReadinessGeneralPercentValue }, ]}
                          width={wp(55)}
                          height={wp(55)}
                          radius={wp(15.25)}
                          colorScale={[myContext.ReadinessGeneralColorValue, "transparent"]}
                          innerRadius={wp(16.75)}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />

                        
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(6), fill: 'white'}}
                          x={wp(27.5)}
                          y={wp(25)}
                          
                          text={[myContext.ReadinessGeneralPercentValue+"%"]}
                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3.5), fill: 'white'}}
                          x={wp(22.5)}
                          y={wp(30)}
                          
                          text={["Risk: "]}
                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3.5), fill: myContext.ReadinessGeneralColorValue}}
                          x={wp(32.5)}
                          y={wp(30)}
                          
                          text={[myContext.ReadinessGeneralTitleValue]}
                        />
                      </VictoryContainer>
                      
                    </View>
                    {/*<View style = {{ ...styles.centrify1, flex: 1, paddingBottom: 0, paddingTop: 0, borderWidth: 1 }}>
                      <Text style = {{ fontSize: 15,      }}> {readiness}% </Text>
                      <Text style = {{ fontSize: 15,      }}> {securityLevel} </Text>
                    </View>*/}
                    </View>
                    
                  </View>
                  <View style = {{ ...styles.container, flex: 1, paddingTop: hp(0), flexDirection: 'column', marginRight: windowWidth/50, }}>
                    <View style = {{ ...styles.centrify1, flex: 0.75,}}>
                      <Text style = {{color: 'white', fontSize: wp(2.5)}}>Failed tests: {(myContext.TotalGeneralTestsValue == 0) ? 0 : (myContext.FailedGeneralTestsValue + myContext.FailedWifiTestsValue)} out of {(myContext.TotalGeneralTestsValue == 0 )? 0 : (myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue)}</Text>
                    </View>
                    <View style = {{ ...styles.centrify1, flex: 1, flexDirection: 'row',}}>
                    <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.CritGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue == 0 ? 1 : myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue) - myContext.CritGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#B71C1C','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.CritGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify1, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Critical </Text>
                      </View>

                    </View>

                    <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.HighGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue == 0 ? 1 : myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue) - myContext.HighGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#E65100','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.HighGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify1, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> High </Text>
                      </View>

                    </View>
                    </View>

                    <View style = {{ ...styles.centrify1, flex: 1, flexDirection: 'row',}}>
                    <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.MedGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue== 0 ? 1 : myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue) - myContext.MedGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#F57F17','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.MedGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify1, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Medium </Text>
                      </View>

                    </View>
                    <View style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>

                      <View  style = {{ ...styles.centrify1, flex: 1, borderWidth: 0 }}>
                        <VictoryContainer 
                        x={0}
                        y={0}
                        height={wp(12.5)}
                        width={wp(12.5)}
                      >
                        <VictoryPie
                          
                          animate={{ easing: 'exp',}}
                          data={[{ y: myContext.LowGeneralTestsValue }, { y: (myContext.TotalGeneralTestsValue  + myContext.TotalWifiTestsValue== 0 ? 1 : myContext.TotalGeneralTestsValue + myContext.TotalWifiTestsValue) - myContext.LowGeneralTestsValue }, ]}
                          width={wp(12.5)}
                          height={wp(12.5)}
                          colorScale={['#2E7D32','white']}
                          innerRadius={15}
                          radius={20}
                          labels={labelData}
                          startAngle = {0}
                          endAngle = {360}
                          
                         

                        />
                        <VictoryLabel 
                          textAnchor="middle"
                          style={{ fontSize: wp(3), fill: 'white'}}
                          x={wp(6.25)}
                          y={wp(6.25)}
                          
                          text={myContext.LowGeneralTestsValue}
                        />
                      </VictoryContainer>
                      </View>
                      <View  style = {{ ...styles.centrify1, flex: 2, borderWidth: 0 }}>
                        <Text style = {{ fontSize: wp(3), color: 'white'      }}> Low </Text>
                      </View>

                    </View>
                    </View>
                  </View>
                </View>
      <View style={{ ...styles.container, flex: 1,  marginTop: hp(1),  }}>
        <Text style = {{ fontSize: wp(6), color: '#D3D3D3',   }}>Threat Breakdown</Text>
      
      </View>
      <View style={{ ...styles.container, flex: 1, marginBottom: hp(1)}}>
        <View style = {{ flexDirection: 'row', borderBottomWidth: 1, borderColor: '#00ACEB', marginLeft: wp(2), marginRight: wp(2), paddingBottom: hp(1),}}>
          <View style = {{ flex: 1.5, }}>
            <Text style = {{ fontSize: wp(3.5), color: '#00ACEB'}}>Security Category</Text>
          </View>

          <View style = {{ flex: 2, paddingRight: wp(4)}}>
            <Text style = {{ fontSize: wp(3.5), color: '#00ACEB', textAlign: 'center'}}>Readiness</Text>
          </View>

          <View style = {{ flex: 1, }}>
            <Text style = {{ fontSize: wp(3.5), color: '#00ACEB', textAlign: 'center'}}>Risk</Text>
          </View>
        </View>
      </View>
      
      
      <View style={{...styles.container, flex: 4,}}>
      
        
        <SafeAreaView style={{ ...styles.container,   }}>
          <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingTop: hp(1), paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Device</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.DeviceReadinessPercentBarValue > 15 ? myContext.DeviceReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.DeviceReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.DeviceReadinessPercentBarValue > 33 ? '#F57F17' : myContext.DeviceReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.DeviceReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.DeviceReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.DeviceReadinessPercentBarValue > 33 ? '#F57F17' : myContext.DeviceReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.DeviceReadinessPercentBarValue > 67 ? 'Low' : myContext.DeviceReadinessPercentBarValue > 33 ? 'Med' : myContext.DeviceReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1)}}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Email</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.EmailReadinessPercentBarValue > 15 ? myContext.EmailReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.EmailReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.EmailReadinessPercentBarValue > 33 ? '#F57F17' : myContext.EmailReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.EmailReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.EmailReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.EmailReadinessPercentBarValue > 33 ? '#F57F17' : myContext.EmailReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.EmailReadinessPercentBarValue > 67 ? 'Low' : myContext.EmailReadinessPercentBarValue > 33 ? 'Med' : myContext.EmailReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Wifi</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.WifiReadinessPercentBarValue > 15 ? myContext.WifiReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.WifiReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.WifiReadinessPercentBarValue > 33 ? '#F57F17' : myContext.WifiReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.WifiReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.WifiReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.WifiReadinessPercentBarValue > 33 ? '#F57F17' : myContext.WifiReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.WifiReadinessPercentBarValue > 67 ? 'Low' : myContext.WifiReadinessPercentBarValue > 33 ? 'Med' : myContext.WifiReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Data</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.DataReadinessPercentBarValue > 15 ? myContext.DataReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.DataReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.DataReadinessPercentBarValue > 33 ? '#F57F17' : myContext.DataReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.DataReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.DataReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.DataReadinessPercentBarValue > 33 ? '#F57F17' : myContext.DataReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.DataReadinessPercentBarValue > 67 ? 'Low' : myContext.DataReadinessPercentBarValue > 33 ? 'Med' : myContext.DataReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Firewall</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.FirewallReadinessPercentBarValue > 15 ? myContext.FirewallReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.FirewallReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.FirewallReadinessPercentBarValue > 33 ? '#F57F17' : myContext.FirewallReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.FirewallReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.FirewallReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.FirewallReadinessPercentBarValue > 33 ? '#F57F17' : myContext.FirewallReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.FirewallReadinessPercentBarValue > 67 ? 'Low' : myContext.FirewallReadinessPercentBarValue > 33 ? 'Med' : myContext.FirewallReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Web</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.WebReadinessPercentBarValue > 15 ? myContext.WebReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.WebReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.WebReadinessPercentBarValue > 33 ? '#F57F17' : myContext.WebReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.WebReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.WebReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.WebReadinessPercentBarValue > 33 ? '#F57F17' : myContext.WebReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.WebReadinessPercentBarValue > 67 ? 'Low' : myContext.WebReadinessPercentBarValue > 33 ? 'Med' : myContext.WebReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>

            <View style= {{ ...styles.tableRow, borderColor: bordCol, borderWidth: bordWid, paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, borderColor: bordCol, borderWidth: bordWid, }}>
                <Text style={{color: 'white', fontSize: wp(4)}}>Privacy</Text>
              </View>

              <View style = {{ flex: 2, borderColor: bordCol, borderWidth: bordWid, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.PrivacyReadinessPercentBarValue > 15 ? myContext.PrivacyReadinessPercentBarValue.toString()+'%' : '15%', backgroundColor: myContext.PrivacyReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.PrivacyReadinessPercentBarValue > 33 ? '#F57F17' : myContext.PrivacyReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.PrivacyReadinessPercentBarValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, borderColor: bordCol, borderWidth: bordWid, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.PrivacyReadinessPercentBarValue > 67 ? '#2E7D32' : myContext.PrivacyReadinessPercentBarValue > 33 ? '#F57F17' : myContext.PrivacyReadinessPercentBarValue > 33 ? '#E65100' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.PrivacyReadinessPercentBarValue > 67 ? 'Low' : myContext.PrivacyReadinessPercentBarValue > 33 ? 'Med' : myContext.PrivacyReadinessPercentBarValue > 15 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>
            

            


            

            






          </ScrollView>
        </SafeAreaView>
      


      </View>
      
      
     
      
    
    </View>

    
  );
}


const styles = StyleSheet.create({

  //table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18222E' 
  },
  centrify:{
    justifyContent: 'flex-start',
    width: '100%',
    height: '100%',
  },
  centrify1:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },

  customRoww: {
        flex: 1,
        justifyContent:'center',

        flexDirection: 'row',
       
        
        
        //marginTop: windowHeight/200,
        //marginBottom: windowHeight/200,
        borderRadius: 0,
        backgroundColor: '#18222E',
        elevation: 2,


    },
    
    customRoww_text: {

        paddingBottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },

    category: {
        fontSize: wp(3.5),
        color: 'white', 
             
        
    },
    status: {
        
        fontSize:windowWidth/25,
        
        color: 'white', 
             
    },
    risk: {
      
        fontSize: windowWidth/30,
        
        color: 'white', 
        textAlign: 'center',
    },


    //modal stuff -->


    centrifyy:{
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },
    button: {
      alignItems: "center",
      
      
    },
    modalView: {
      height: hp(50),

      width: wp(80),
      margin: hp(3),
      backgroundColor: "#1C2B3A",
      borderRadius: 20,
      padding: wp(5),
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5
    },
    openButton: {
      backgroundColor: "#F194FF",
      borderRadius: 20,
      padding: wp(3),
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center",
      
      
           
    },
    modalText: {
      marginTop: hp(5),
      marginBottom: hp(3),
      textAlign: "center",
      fontSize: wp(5),
      color: '#D3D3D3'
           
    },

    progressBarContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      //paddingTop: Constants.statusBarHeight,
      backgroundColor: '#ecf0f1',
      padding: wp(3),
    },
    progressBar: {
      margin: wp(3),
    },
    centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(4),
  },

    //<-- modal stuff
  
});