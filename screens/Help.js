import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { FlatList, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppContext from "../components/AppContext";
import DeviceInfo from 'react-native-device-info';
import { getUniqueId, getManufacturer } from 'react-native-device-info';
//import { useFonts } from '@use-expo/font';

//import { AppLoading } from 'expo';
//import { Table, TableWrapper, Cell, Row, Rows } from 'react-native-table-component';

const Stack = createStackNavigator();

import {NativeModules} from 'react-native';
var TestModule = NativeModules.TestModule;



import { Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let bordCol = "#00ACEB";
let bordWid = 1




function HelpScreen({navigation}) {

  // let [fontsLoaded] = useFonts({
  //   'DidactGothic-Regular': require('../assets/fonts/DidactGothic-Regular.ttf'),
  // });
  const myContext = useContext(AppContext);

 
  


  
  
  return (
    

    <View style={{ ...styles.container, flex: 1, }}>
      
      

      

      
        <SafeAreaView style={{ ...styles.container,   }}>
          <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

            <Image
                style={{ height: windowHeight, width: windowWidth}}
                source={require("../assets/images/help1.png")}
              />
              <Image
                style={{ height: windowHeight, width: windowWidth}}
                source={require("../assets/images/help2.png")}
              />
              <Image
                style={{ height: windowHeight, width: windowWidth}}
                source={require("../assets/images/help3.png")}
              />
              <Image
                style={{ height: windowHeight, width: windowWidth}}
                source={require("../assets/images/help4.png")}
              />

            


            

            






          </ScrollView>
        </SafeAreaView>
      
      
     
      
      

      {/*<View style={{ ...styles.container, flex: 0.75 }}>
        <Text style = {{ fontSize: windowWidth/25, textAlign: 'center', color: 'white',    }}> WhiteHax App on each Device can provide security risk remediation steps </Text>
      </View>*/}
       

    </View>

    
  );

};

function HeaderComponent(){
  return(
    
    <Text style= {{ fontSize: windowHeight/30, color:'#FFFFFF' }}>White<Text style= {{ fontSize: windowHeight/30, color:'#FE0000' }}>HaX</Text></Text>
    
  )
}
export default function Help({ navigation }) {
  return (

    <Stack.Navigator>

      <Stack.Screen name="Help" component={HelpScreen} options={{
          
          headerTitleStyle: {
            fontSize: windowWidth/10,

            color: 'red',
          },
          headerStyle: {
            backgroundColor: '#1C2B3A',
            height: windowHeight/7.5,
          },
          title: HeaderComponent(),
          headerLeft: () => (
            <TouchableWithoutFeedback  onPress={() => navigation.toggleDrawer()}>
              <View style = {{ paddingLeft: windowWidth/30, }}>
                <Image
                  style={{ width: 35, height: 35, }}
                  source={require('../assets/android/4x/top_bar_menu_iconxxxhdpi.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            
          ),
        }}/>
      
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18222E' 
  },
  centrify:{
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  customRoww: {
        flex: 1,
        justifyContent:'center',

        flexDirection: 'row',
       
        
        
        //marginTop: windowHeight/200,
        //marginBottom: windowHeight/200,
        borderRadius: 0,
        backgroundColor: '#18222E',
        elevation: 2,


    },
    
    customRoww_text: {

        //paddingBottom: windowHeight/400,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },
    firstCustomRowText: {

        //paddingBottom: windowHeight/20,
        //paddingTop: windowHeight/20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },

    category: {
        fontSize:wp(3.5) ,
        color: 'white',
              
        
    },
    status: {
        
        fontSize:15,
        
        color: 'white', 
             
    },
    risk: {
      
        fontSize: wp(3),
        
        color: 'white', 
        textAlign: 'center',
             
    },
    //modal stuff -->
    centrifyy:{
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },

    button: {
      alignItems: "center",
      
      
    },

    //<-- modal stuff

    body: {
    flex: 1,
    padding: 10, 
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  
  
});