import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { PermissionsAndroid, FlatList, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppContext from "../components/AppContext";
import DeviceInfo from 'react-native-device-info';
import { getUniqueId, getManufacturer } from 'react-native-device-info';

const requestCameraPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};
//import { useFonts } from '@use-expo/font';

//import { AppLoading } from 'expo';
//import { Table, TableWrapper, Cell, Row, Rows } from 'react-native-table-component';

const Stack = createStackNavigator();

import {NativeModules} from 'react-native';
var TestModule = NativeModules.TestModule;



import { Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let bordCol = "#00ACEB";
let bordWid = 1




function ListScreen({navigation}) {

 
  const myContext = useContext(AppContext);

  let i;
  const [tempData, setTempData] = useState('n/a')

  
  
  let [NonWifiLoop, setNonWifiLoop] = useState([])
  let [WifiLoop, setWifiLoop] = useState([])
  

  

  function getTestValue() {

    let tempNonWifiLoop = []
    let tempDisplayName;
    let i, j;
    let configJSON = {"profile_subcategory_mappings":[{"id":4001,"test_name":"lockPasswordEnable","attack_name":"WH_DeviceLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Device Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4002,"test_name":"","attack_name":"WH_SmartLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":6,"readiness_type":"Device","display_name":"Smart Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4003,"test_name":"isLockPasswordStrong","attack_name":"WH_Passwd_Check","estimated_time":30,"decription":"You should use a strong passwd for Device and Data protection","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Weak Password","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4004,"test_name":"isGooglePlayProtectEnable","attack_name":"WH_Google_Play_Protect","estimated_time":30,"decription":"Google Play Protect provides application verification","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Application Verification","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4005,"test_name":"","attack_name":"WH_SW_Version_Check","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","remedy_str":"NA","weight":25,"readiness_type":"Device","display_name":"Android OS Version","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4006,"test_name":"","attack_name":"WH_SW_Update_ON","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","weight":5,"readiness_type":"Device","display_name":"Android OS Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4007,"test_name":"defaultBrowserSafeCheck","attack_name":"WH_Browser_Default","estimated_time":30,"decription":"Use a browser that provides strong privacy and data protection","security_soln":"Web Security","weight":7,"readiness_type":"Web","display_name":"Browser Type","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4008,"test_name":"defaultBrowserUpdated","attack_name":"WH_Browser_Version","estimated_time":30,"decription":"Keep you Browser up-to-date to get latest security features","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Broswer Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4009,"test_name":"","attack_name":"WH_Safe_Browsing_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Safe Browser","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4010,"test_name":"webProxy","attack_name":"WH_Web_Proxy_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Web Proxy for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4011,"test_name":"","attack_name":"WH_Web_VPN_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Web VPN for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4012,"test_name":"","attack_name":"WH_2FA_Auth_Check","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Device","display_name":"2 Factor Authentication","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4013,"test_name":"","attack_name":"WH_Google_Account_Security","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Data","display_name":"Google Account Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4014,"test_name":"camerLoctionPermission","attack_name":"WH_Privacy_Loc_Share_Cam","estimated_time":30,"decription":"Turn off Location Sharing when using Camera to protect privacy","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Camera Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4015,"test_name":"isAppLocationSharingPermission","attack_name":"WH_Privacy_Loc_Share_Apps","estimated_time":30,"decription":"Turn off Location Sharing when using Social Media to protect privacy","security_soln":"Privacy","weight":5,"readiness_type":"Privacy","display_name":"Privacy - App Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4016,"test_name":"isAppLocationSharingSocialPermission","attack_name":"WH_Privacy_SocMed_Loc_Share","estimated_time":30,"decription":"Turn on Privacy and security settings when using Social Media Apps","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Social Media Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4017,"test_name":"weakFireWallApp","attack_name":"WH_FW_Installed","estimated_time":30,"decription":"Firewall protects your Device communication","security_soln":"Firewall","weight":8,"readiness_type":"Firewall","display_name":"Weak Firewall","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4018,"test_name":"antiPhishingApp","attack_name":"WH_AntiPhishing_Installed","estimated_time":30,"decription":"You are missing Email protection against Phishing ","security_soln":"Email","weight":6,"readiness_type":"Email","display_name":"Weak Email Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4019,"test_name":"antiVirusAppCheck","attack_name":"WH_AntiVirus_Installed","estimated_time":30,"decription":"You are missing  protection against Viruses and Malware ","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Weak Virus Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4020,"test_name":"vPNAppAvailable","attack_name":"WH_VPN_App_Check","estimated_time":30,"decription":"You are missing  VPN for enhance privacy and security while browsing Admin User access can cause many types of data and security breach","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Blocking ISP Data Snooping","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4021,"test_name":"","attack_name":"WH_DataExfiltration_Cookie_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":7,"readiness_type":"Data","display_name":"Data Stealing thru Cookies","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4022,"test_name":"","attack_name":"WH_DataExfiltration_Dir_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":5,"readiness_type":"Data","display_name":"Data Stealing thru Files","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4023,"test_name":"deviceRootcheck1","attack_name":"WH_OS_Rooted_Check1","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4024,"test_name":"deviceRootcheck2","attack_name":"WH_OS_Rooted_Check2","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4025,"test_name":"deviceRootcheck3","attack_name":"WH_OS_Rooted_Check3","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4027,"test_name":"isHiddenSSID","attack_name":"WH_Wifi_SSID_Visible","estimated_time":30,"decription":"Hide WiFi SSID to protect your WiFi against intrusions","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi SSID Pubicly Visible","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4028,"test_name":"isWifiEncrypted","attack_name":"WH_Wifi_Encrypt_Strength","estimated_time":30,"decription":"Use strong WiFI Encryption for stronger security","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"Weak WiFi Encryption","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4029,"test_name":"isIpDefaultRouter","attack_name":"WH_Wifi_Router_IP_Default","estimated_time":30,"decription":"Using vendor default IP increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"WiFi Security","display_name":"WiFi uses Vendor Default","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4030,"test_name":"isRouterAdminAccess","attack_name":"WH_Wifi_Router_Admin_Access","estimated_time":30,"decription":"Using vendor default Router passwd increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"WiFi Admin Access Break-in","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4031,"test_name":"isport1Available","attack_name":"WH_Wifi_FW_Port1","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"FirFirewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4032,"test_name":"isport2Available","attack_name":"WH_Wifi_FW_Port2","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4033,"test_name":"isport3Available","attack_name":"WH_Wifi_FW_Port3","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4034,"test_name":"isport4Available","attack_name":"WH_Wifi_FW_Port4","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4035,"test_name":"dnsPos","attack_name":"WH_Wifi_DNS_Poisoning","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi DNS Poisoning Risk","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4036,"test_name":"eicarFileCheck","attack_name":"WH_EICAR","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4037,"test_name":"eicarFileCheck1","attack_name":"WH_EICAR1","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4038,"test_name":"eicarFileCheck2","attack_name":"WH_EICAR2","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4039,"test_name":"eicarFileCheck3","attack_name":"WH_EICAR3","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4040,"test_name":"eicarFileCheck4","attack_name":"WH_EICAR4","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4041,"test_name":"eicarFileCheck5","attack_name":"WH_EICAR5","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4042,"test_name":"vpnNat","attack_name":"WH_Wifi_VPN_NAT","estimated_time":30,"decription":"You VPN over WiFi to increase Data Security and Privacy","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"No WiFi VPN","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}}]}
    

    TestModule.getString((info) => {
      //setGen(JSON.parse(info));
      //console.log(info.deviceInfo);
      console.log(info)

      

      for (i  = 0 ; i < JSON.parse(info).generalChecks.length ; i++) {
        tempDisplayName = JSON.parse(info).generalChecks[i].name;
        for( j = 0; j < configJSON.profile_subcategory_mappings.length ; j++) {
          if ( configJSON.profile_subcategory_mappings[j].test_name == JSON.parse(info).generalChecks[i].name) {
            tempDisplayName = configJSON.profile_subcategory_mappings[j].attack_name;
          }
          
        }


        tempNonWifiLoop.push(

          <View key = {i} style={{ ...styles.body }}>
            <View style={{ flex: 1, borderBottomWidth: 0.5, borderColor: 'white', width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 4}}> 
                <Text style = {{color: 'white'}}>{i+1}. {tempDisplayName}</Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text> {JSON.parse(info).generalChecks[i].status == 'PASS' ? <Text style={{color:'green'}}> Pass </Text> : JSON.parse(info).generalChecks[i].status == ''? <Text style={{color:'white'}}> n/a </Text>:<Text style={{color:'red'}}> Fail </Text> } </Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style={{textAlign: 'center', color: JSON.parse(info).generalChecks[i].weight >= 9 ? '#B71C1C' : JSON.parse(info).generalChecks[i].weight >= 6 ? '#E65100' : JSON.parse(info).generalChecks[i].weight >= 4 ? '#F57F17' : '#2E7D32'}}>{JSON.parse(info).generalChecks[i].weight >= 9 ? "Crit" : JSON.parse(info).generalChecks[i].weight >= 6 ? "High" : JSON.parse(info).generalChecks[i].weight >= 4 ? "Med" : "Low"}</Text>
              </View>
            </View>
          </View>

        )
      }
      //console.log(tempNonWifiLoop)
      
    });

    TestModule.getGooglePlayProtect((GPPinfo) => {
        console.log(typeof(GPPinfo))
      tempNonWifiLoop.push(

          <View key = {i+1} style={{ ...styles.body }}>
            <View style={{ flex: 1, borderBottomWidth: 0.5, borderColor: 'white', width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 4}}> 
                <Text style = {{color: 'white'}}>{i+1}. WH_Google_Play_Protect</Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text> {GPPinfo == 'PASS' ? <Text style={{color:'green', textAlign: 'center'}}> Pass </Text> : GPPinfo == ''? <Text style={{color:'white'}}> n/a </Text>:<Text style={{color:'red', textAlign: 'center'}}> Fail </Text> } </Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style = {{color: '#E65100', textAlign: 'center'}}>High</Text> 
              </View>
            </View>
          </View>

        )
      setNonWifiLoop(tempNonWifiLoop)
    })

    
  }

  
  function getWifiValue() {

    let tempWifiLoop = []
    let tempDisplayName;
    let i, j;
    
    let configJSON = {"profile_subcategory_mappings":[{"id":4001,"test_name":"lockPasswordEnable","attack_name":"WH_DeviceLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Device Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4002,"test_name":"","attack_name":"WH_SmartLock_Check","estimated_time":30,"decription":"Locking the Device when not in use, prevents misuse","security_soln":"OS and User","weight":6,"readiness_type":"Device","display_name":"Smart Lock","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4003,"test_name":"isLockPasswordStrong","attack_name":"WH_Passwd_Check","estimated_time":30,"decription":"You should use a strong passwd for Device and Data protection","security_soln":"OS and User","weight":9,"readiness_type":"Device","display_name":"Weak Password","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4004,"test_name":"isGooglePlayProtectEnable","attack_name":"WH_Google_Play_Protect","estimated_time":30,"decription":"Google Play Protect provides application verification","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Application Verification","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4005,"test_name":"","attack_name":"WH_SW_Version_Check","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","remedy_str":"NA","weight":25,"readiness_type":"Device","display_name":"Android OS Version","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4006,"test_name":"","attack_name":"WH_SW_Update_ON","estimated_time":30,"decription":"Keep you Android OS up-to-date to get latest security features","security_soln":"OS and User","weight":5,"readiness_type":"Device","display_name":"Android OS Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4007,"test_name":"defaultBrowserSafeCheck","attack_name":"WH_Browser_Default","estimated_time":30,"decription":"Use a browser that provides strong privacy and data protection","security_soln":"Web Security","weight":7,"readiness_type":"Web","display_name":"Browser Type","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4008,"test_name":"defaultBrowserUpdated","attack_name":"WH_Browser_Version","estimated_time":30,"decription":"Keep you Browser up-to-date to get latest security features","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Broswer Update","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4009,"test_name":"","attack_name":"WH_Safe_Browsing_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Safe Browser","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4010,"test_name":"webProxy","attack_name":"WH_Web_Proxy_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":6,"readiness_type":"Web","display_name":"Web Proxy for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4011,"test_name":"","attack_name":"WH_Web_VPN_Check","estimated_time":30,"decription":"NA","security_soln":"Web Security","weight":0,"readiness_type":"Web","display_name":"Web VPN for Web Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4012,"test_name":"","attack_name":"WH_2FA_Auth_Check","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Device","display_name":"2 Factor Authentication","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4013,"test_name":"","attack_name":"WH_Google_Account_Security","estimated_time":30,"decription":"NA","security_soln":"OS and User","weight":0,"readiness_type":"Data","display_name":"Google Account Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4014,"test_name":"camerLoctionPermission","attack_name":"WH_Privacy_Loc_Share_Cam","estimated_time":30,"decription":"Turn off Location Sharing when using Camera to protect privacy","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Camera Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4015,"test_name":"isAppLocationSharingPermission","attack_name":"WH_Privacy_Loc_Share_Apps","estimated_time":30,"decription":"Turn off Location Sharing when using Social Media to protect privacy","security_soln":"Privacy","weight":5,"readiness_type":"Privacy","display_name":"Privacy - App Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4016,"test_name":"isAppLocationSharingSocialPermission","attack_name":"WH_Privacy_SocMed_Loc_Share","estimated_time":30,"decription":"Turn on Privacy and security settings when using Social Media Apps","security_soln":"Privacy","weight":6,"readiness_type":"Privacy","display_name":"Privacy - Social Media Location Sharing","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4017,"test_name":"weakFireWallApp","attack_name":"WH_FW_Installed","estimated_time":30,"decription":"Firewall protects your Device communication","security_soln":"Firewall","weight":8,"readiness_type":"Firewall","display_name":"Weak Firewall","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4018,"test_name":"antiPhishingApp","attack_name":"WH_AntiPhishing_Installed","estimated_time":30,"decription":"You are missing Email protection against Phishing ","security_soln":"Email","weight":6,"readiness_type":"Email","display_name":"Weak Email Security","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4019,"test_name":"antiVirusAppCheck","attack_name":"WH_AntiVirus_Installed","estimated_time":30,"decription":"You are missing  protection against Viruses and Malware ","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Weak Virus Protection","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4020,"test_name":"vPNAppAvailable","attack_name":"WH_VPN_App_Check","estimated_time":30,"decription":"You are missing  VPN for enhance privacy and security while browsing Admin User access can cause many types of data and security breach","security_soln":"Email, Web","weight":7,"readiness_type":"Email, Web","display_name":"Blocking ISP Data Snooping","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4021,"test_name":"","attack_name":"WH_DataExfiltration_Cookie_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":7,"readiness_type":"Data","display_name":"Data Stealing thru Cookies","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4022,"test_name":"","attack_name":"WH_DataExfiltration_Dir_Scan","estimated_time":30,"decription":"NA","security_soln":"Data","weight":5,"readiness_type":"Data","display_name":"Data Stealing thru Files","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4023,"test_name":"deviceRootcheck1","attack_name":"WH_OS_Rooted_Check1","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4024,"test_name":"deviceRootcheck2","attack_name":"WH_OS_Rooted_Check2","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4025,"test_name":"deviceRootcheck3","attack_name":"WH_OS_Rooted_Check3","estimated_time":30,"decription":"Admin User access can cause many types of data and security breach","security_soln":"OS and User","weight":7,"readiness_type":"Device","display_name":"Potential 3rd-party Device Control","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4027,"test_name":"isHiddenSSID","attack_name":"WH_Wifi_SSID_Visible","estimated_time":30,"decription":"Hide WiFi SSID to protect your WiFi against intrusions","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi SSID Pubicly Visible","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4028,"test_name":"isWifiEncrypted","attack_name":"WH_Wifi_Encrypt_Strength","estimated_time":30,"decription":"Use strong WiFI Encryption for stronger security","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"Weak WiFi Encryption","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4029,"test_name":"isIpDefaultRouter","attack_name":"WH_Wifi_Router_IP_Default","estimated_time":30,"decription":"Using vendor default IP increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"WiFi Security","display_name":"WiFi uses Vendor Default","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4030,"test_name":"isRouterAdminAccess","attack_name":"WH_Wifi_Router_Admin_Access","estimated_time":30,"decription":"Using vendor default Router passwd increases WiFi break-in risks","security_soln":"WiFi Router/FW","weight":9,"readiness_type":"WiFi Security","display_name":"WiFi Admin Access Break-in","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4031,"test_name":"isport1Available","attack_name":"WH_Wifi_FW_Port1","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"FirFirewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4032,"test_name":"isport2Available","attack_name":"WH_Wifi_FW_Port2","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4033,"test_name":"isport3Available","attack_name":"WH_Wifi_FW_Port3","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4034,"test_name":"isport4Available","attack_name":"WH_Wifi_FW_Port4","estimated_time":30,"decription":"Do not allow inbound connections from Internet","security_soln":"WiFi Router/FW","weight":7,"readiness_type":"Firewall","display_name":"Firewall Inbound Access","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4035,"test_name":"dnsPos","attack_name":"WH_Wifi_DNS_Poisoning","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi DNS Poisoning Risk","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4036,"test_name":"eicarFileCheck","attack_name":"WH_EICAR","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4037,"test_name":"eicarFileCheck1","attack_name":"WH_EICAR1","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4038,"test_name":"eicarFileCheck2","attack_name":"WH_EICAR2","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4039,"test_name":"eicarFileCheck3","attack_name":"WH_EICAR3","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4040,"test_name":"eicarFileCheck4","attack_name":"WH_EICAR4","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4041,"test_name":"eicarFileCheck5","attack_name":"WH_EICAR5","estimated_time":30,"decription":"Your WiFi accesses potentially compromised DNS server","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"WiFi EICAR File Check","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}},{"id":4042,"test_name":"vpnNat","attack_name":"WH_Wifi_VPN_NAT","estimated_time":30,"decription":"You VPN over WiFi to increase Data Security and Privacy","security_soln":"WiFi Router/FW","weight":6,"readiness_type":"WiFi Security","display_name":"No WiFi VPN","active":true,"category":"","remediation":{"auto":true,"steps_list":[],"function":{"function_name":"","function_parameters":""}}}]}
    TestModule.getWifi((info) => {
      //setWifi(JSON.parse(info));
      console.log(info);

      for (i  = 0 ; i < JSON.parse(info).wifiCheck.length ; i++) {
        tempDisplayName = JSON.parse(info).wifiCheck[i].name;
        for( j = 0; j < configJSON.profile_subcategory_mappings.length ; j++) {
          if ( configJSON.profile_subcategory_mappings[j].test_name == JSON.parse(info).wifiCheck[i].name) {
            tempDisplayName = configJSON.profile_subcategory_mappings[j].attack_name;
          }
          

        }
        tempWifiLoop.push(

          <View key = {i} style={{ ...styles.body }}>
            <View style={{ flex: 1, borderBottomWidth: 0.5, borderColor: 'white', width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 4}}> 
                <Text style = {{color: 'white'}}>{i+1}. {tempDisplayName}</Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text> {JSON.parse(info).wifiCheck[i].value == 'PASS' ? <Text style={{color:'green'}}> Pass </Text> : JSON.parse(info).wifiCheck[i].value == ''? <Text style={{color:'white'}}> n/a </Text>:<Text style={{color:'red'}}> Fail </Text> } </Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style={{textAlign: 'center', color: JSON.parse(info).wifiCheck[i].weight >= 9 ? '#B71C1C' : JSON.parse(info).wifiCheck[i].weight >= 6 ? '#E65100' : JSON.parse(info).wifiCheck[i].weight >= 4 ? '#F57F17' : '#2E7D32'}}>{JSON.parse(info).wifiCheck[i].weight >= 9 ? "Crit" : JSON.parse(info).wifiCheck[i].weight >= 6 ? "High" : JSON.parse(info).wifiCheck[i].weight >= 4 ? "Med" : "Low"}</Text>
              </View>
            </View>
          </View>

        )
      }
      //console.log(tempNonWifiLoop)
      setWifiLoop([tempWifiLoop])
    });
  }
  

  


  
  
  return (
    

    <View style={{ ...styles.container, flex: 1, }}>
      
      

      

      <View style={{ ...styles.container, flex: 5 }}>
        <SafeAreaView style={{ ...styles.container,   }}>
          <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

            <View style={{ flex: 1, height: '100%', width: '100%', justifyContent: 'center',  alignItems: 'center', }}>
          
          <View style={styles.body}>
            <View style={{ flex: 1, borderColor: 'white',marginTop: 20, width: '100%'}}>
              <TouchableOpacity
                onPress = {() => {
                  getTestValue()
                }}
              >
                <View style={{ flex: 1, width: '100%', height: '100%', padding: 10, backgroundColor: '#64b5f6'}}>
                  <Text style= {{textAlign: 'center'}}> Press For Non-Wifi Test Info </Text>
                </View>            
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.body}>
            <View style={{ flex: 1, borderBottomWidth: 2, borderColor: 'white', width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 4}}> 
                <Text style= {{ color: 'white'}}> Test</Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style= {{ color: 'white'}}> Status </Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style= {{ color: 'white'}}> Severity </Text> 
              </View>
            </View>
          </View>

          {NonWifiLoop}

          

          <View style={styles.body}>
            <View style={{ flex: 1,marginTop: 20, width: '100%'}}>
              <TouchableOpacity
                onPress = {getWifiValue}
              >
                <View style={{ flex: 1, width: '100%', height: '100%', padding: 10, backgroundColor: '#64b5f6'}}>
                  <Text style= {{textAlign: 'center'}}> Press For Wifi Test Info </Text>
                </View>            
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.body}>
            <View style={{ flex: 1, borderBottomWidth: 2, borderColor: 'white', width: '100%', flexDirection: 'row'}}>
              <View style={{flex: 4}}> 
                <Text style = {{color: 'white'}}> Test</Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style = {{color: 'white'}}> Status </Text> 
              </View>
              <View style={{flex: 1}}> 
                <Text style = {{color: 'white'}}> Severity </Text> 
              </View>
            </View>
          </View>

          {WifiLoop}


        </View>
            

            


            

            






          </ScrollView>
        </SafeAreaView>
      
      </View>
     
      
      

      {/*<View style={{ ...styles.container, flex: 0.75 }}>
        <Text style = {{ fontSize: windowWidth/25, textAlign: 'center', color: 'white',    }}> WhiteHax App on each Device can provide security risk remediation steps </Text>
      </View>*/}
       

    </View>

    
  );

};

function HeaderComponent(){
  return(
    
    <Text style= {{ fontSize: windowHeight/30, color:'#FFFFFF' }}>White<Text style= {{ fontSize: windowHeight/30, color:'#FE0000' }}>HaX</Text></Text>
    
  )
}
export default function ListOfTests({ navigation }) {
  return (

    <Stack.Navigator>

      <Stack.Screen name="List of Tests" component={ListScreen} options={{
          
          headerTitleStyle: {
            fontSize: windowWidth/10,

            color: 'red',
          },
          headerStyle: {
            backgroundColor: '#1C2B3A',
            height: windowHeight/7.5,
          },
          title: HeaderComponent(),
          headerLeft: () => (
            <TouchableWithoutFeedback  onPress={() => navigation.toggleDrawer()}>
              <View style = {{ paddingLeft: windowWidth/30, }}>
                <Image
                  style={{ width: 35, height: 35, }}
                  source={require('../assets/android/4x/top_bar_menu_iconxxxhdpi.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            
          ),
        }}/>
      
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18222E' 
  },
  centrify:{
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  customRoww: {
        flex: 1,
        justifyContent:'center',

        flexDirection: 'row',
       
        
        
        //marginTop: windowHeight/200,
        //marginBottom: windowHeight/200,
        borderRadius: 0,
        backgroundColor: '#18222E',
        elevation: 2,


    },
    
    customRoww_text: {

        //paddingBottom: windowHeight/400,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },
    firstCustomRowText: {

        //paddingBottom: windowHeight/20,
        //paddingTop: windowHeight/20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },

    category: {
        fontSize:wp(3.5) ,
        color: 'white',
              
        
    },
    status: {
        
        fontSize:15,
        
        color: 'white', 
             
    },
    risk: {
      
        fontSize: wp(3),
        
        color: 'white', 
        textAlign: 'center',
             
    },
    //modal stuff -->
    centrifyy:{
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },

    button: {
      alignItems: "center",
      
      
    },

    //<-- modal stuff

    body: {
    flex: 1,
    padding: 10, 
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  
  
});