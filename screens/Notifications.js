import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { Linking, FlatList, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppContext from "../components/AppContext";
import DeviceInfo from 'react-native-device-info';
import { getUniqueId, getManufacturer } from 'react-native-device-info';
//import { useFonts } from '@use-expo/font';

//import { AppLoading } from 'expo';
//import { Table, TableWrapper, Cell, Row, Rows } from 'react-native-table-component';

const Stack = createStackNavigator();

import {NativeModules} from 'react-native';
var TestModule = NativeModules.TestModule;



import { Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let bordCol = "#00ACEB";
let bordWid = 0




function NotificationsScreen({navigation}) {


  const [ModalArray, setModalArray] = useState([false,false,false,false])
  const myContext = useContext(AppContext);

  let i;
  const [tempData, setTempData] = useState('n/a')

  
  
  let [NonWifiLoop, setNonWifiLoop] = useState([])
  let [WifiLoop, setWifiLoop] = useState([])


  

  

  

  


  
  
  return (
    

    <View style={{ ...styles.container, flex: 1, }}>
      
      <Modal
          animationType="slide"
          transparent={true}
          visible={ModalArray[0]}
      >
        <View style={styles.centeredView_}>
          <View style={styles.modalView_}>
            
            <View style = {{ ...styles.centrify_, paddingBottom: hp(2),flex: 1, backgroundColor: '#1C2B3A', borderBottomWidth: 0.5, borderColor: 'white' }} >
              <Text style={styles.modalText_}>Crypto Currency Scam</Text>
            </View>

            <View style = {{ ...styles.centrify_,  paddingTop: hp(2), flex: 8, backgroundColor: '#1C2B3A', borderColor: 'white', borderWidth: bordWid, }} >
              
              <SafeAreaView style={{ ...styles.container_,   }}>
                <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

                  <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text style= {{...styles.modalText_, textAlign: 'left', fontSize: wp(3)}} > Cryptocurrency scams are now a popular way for scammers to trick people into sending money. And they pop up in many ways. Most crypto scams can appear as emails trying to blackmail someone, online chain referral schemes, or bogus investment and business opportunities. But here’s what they all have in common – and what they have in common with yesterday’s Twitter hacks: A scammer wants you to send money, or make a payment, with Bitcoin or another type of cryptocurrency. Once you do, your money is gone, and there’s generally no way to get it back. </Text>
                    <Text style= {{...styles.modalText_, textAlign: 'left', fontSize: wp(3)}} >So if you see a tweet (or a text, email, or other message on social media) that tells you to pay with Bitcoin, you know that’s a scam. Other signs that something’s a scam? They might guarantee that you’ll make money (those guarantees are false); promise that you’ll double your money quickly (again, that’s always a fake promise)  </Text>

                  </View>
                  <View style={{marginTop: hp(2), justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text 
                      style={{...styles.modalText_, fontSize: wp(4), textDecorationLine: 'underline', fontStyle: 'italic'}}
                      onPress={() => Linking.openURL('https://www.consumer.ftc.gov/blog/2020/07/avoiding-cryptocurrency-scam')}
                    >
                        Click to visit website
                    </Text>

                  </View>

                </ScrollView>
              </SafeAreaView>
              
            </View>
            <View style = {{ ...styles.centrify_, flex: 1, backgroundColor: '#1C2B3A',  }} >
              <TouchableHighlight
                style={{ ...styles.openButton_, backgroundColor: "#00ACEB", }}
                onPress={() => {setModalArray([false,false,false,false])}}
                //disabled = {runButton}
              >
                <Text style={styles.textStyle_}>     Close     </Text>
              </TouchableHighlight>                
            </View>
          </View>
        </View>
      </Modal>

      <Modal
          animationType="slide"
          transparent={true}
          visible={ModalArray[1]}
      >
        <View style={styles.centeredView_}>
          <View style={styles.modalView_}>
            
            <View style = {{ ...styles.centrify_,  paddingBottom: hp(2),flex: 1, backgroundColor: '#1C2B3A', borderBottomWidth: 0.5, borderColor: 'white' }} >
              <Text style={styles.modalText_}>Medicare Beneficiaries Free Braces Scam</Text>
            </View>

            <View style = {{ ...styles.centrify_, paddingTop: hp(2), flex: 8, backgroundColor: '#1C2B3A', borderColor: 'white', borderWidth: bordWid, }} >
              
              <SafeAreaView style={{ ...styles.container_,   }}>
                <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

                  <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text style= {{...styles.modalText_, textAlign: 'left', fontSize: wp(3)}} > The U.S. Department of Health and Human Services Office of Inspector General is alerting the public about a fraud scheme involving orthotic braces and other medical equipment.

Scammers are offering Medicare beneficiaries orthotic braces that are supposedly free to them and covered by Medicare. Fraudsters (1) may call beneficiaries directly to offer braces or (2) use television or radio advertisements to encourage beneficiaries to order free braces by calling the phone number provided.

If a beneficiary provides or verifies personal or Medicare information, a brace is sent even if it is not requested by the beneficiary or medically necessary. Often, a beneficiary receives multiple braces. Medicare is then billed for each brace using the beneficiary's information.

If a beneficiary has received unwanted or unneeded braces, and that equipment is billed to Medicare, then Medicare may deny a brace that the beneficiary needs in the future. </Text>
                    

                  </View>
                  <View style={{marginTop: hp(2), justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text 
                      style={{...styles.modalText_, fontSize: wp(4), textDecorationLine: 'underline', fontStyle: 'italic'}}
                      onPress={() => Linking.openURL('https://oig.hhs.gov/fraud/consumer-alerts/alerts/bracescam.asp')}
                    >
                        Click to visit website
                    </Text>

                  </View>

                </ScrollView>
              </SafeAreaView>
              
            </View>
            <View style = {{ ...styles.centrify_, flex: 1, backgroundColor: '#1C2B3A',  }} >
              <TouchableHighlight
                style={{ ...styles.openButton_, backgroundColor: "#00ACEB", }}
                onPress={() => {setModalArray([false,false,false,false])}}
                //disabled = {runButton}
              >
                <Text style={styles.textStyle_}>     Close     </Text>
              </TouchableHighlight>                
            </View>
          </View>
        </View>
      </Modal>

      <Modal
          animationType="slide"
          transparent={true}
          visible={ModalArray[2]}
      >
        <View style={styles.centeredView_}>
          <View style={styles.modalView_}>
            
            <View style = {{ ...styles.centrify_,  paddingBottom: hp(2),flex: 1, backgroundColor: '#1C2B3A', borderBottomWidth: 0.5, borderColor: 'white' }} >
              <Text style={styles.modalText_}>Customer Service Scams</Text>
            </View>

            <View style = {{ ...styles.centrify_, paddingTop: hp(2), flex: 8, backgroundColor: '#1C2B3A', borderColor: 'white', borderWidth: bordWid, }} >
              
              <SafeAreaView style={{ ...styles.container_,   }}>
                <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

                  <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text style= {{...styles.modalText_, textAlign: 'left', fontSize: wp(3)}} > If you want to contact a company’s customer service department, you can do a quick search online and often find what looks like its phone number or email. But is the information at the top of your search results actually correct?

Based on reports the FTC has gotten, sometimes the answer to that question is: no. Some scammers are creating fake customer service information for popular companies and paying for it to show up in your search results. When you contact them, they’ll offer to “resolve” the problem you may have — if you wire money to them or send gift cards. They might also ask for your personal information, or to get remote access to your computer.

</Text>
                    

                  </View>
                  <View style={{marginTop: hp(2), justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text 
                      style={{...styles.modalText_, fontSize: wp(4), textDecorationLine: 'underline', fontStyle: 'italic'}}
                      onPress={() => Linking.openURL('https://www.consumer.ftc.gov/blog/2020/08/scammers-and-customer-service-another-imposter-scam')}
                    >
                        Click to visit website
                    </Text>

                  </View>

                </ScrollView>
              </SafeAreaView>
              
            </View>
            <View style = {{ ...styles.centrify_, flex: 1, backgroundColor: '#1C2B3A',  }} >
              <TouchableHighlight
                style={{ ...styles.openButton_, backgroundColor: "#00ACEB", }}
                onPress={() => {setModalArray([false,false,false,false])}}
                //disabled = {runButton}
              >
                <Text style={styles.textStyle_}>     Close     </Text>
              </TouchableHighlight>                
            </View>
          </View>
        </View>
      </Modal>


      <Modal
          animationType="slide"
          transparent={true}
          visible={ModalArray[3]}
      >
        <View style={styles.centeredView_}>
          <View style={styles.modalView_}>
            
            <View style = {{ ...styles.centrify_,  paddingBottom: hp(2),flex: 1, backgroundColor: '#1C2B3A', borderBottomWidth: 0.5, borderColor: 'white' }} >
              <Text style={styles.modalText_}>Official Business Impersonation</Text>
            </View>

            <View style = {{ ...styles.centrify_, paddingTop: hp(2), flex: 8, backgroundColor: '#1C2B3A', borderColor: 'white', borderWidth: bordWid, }} >
              
              <SafeAreaView style={{ ...styles.container_,   }}>
                <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>

                  <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text style= {{...styles.modalText_, textAlign: 'left', fontSize: wp(3)}} > Scammers love to use the same old tricks in new ways. One of their favorites is to pose as a business or government official to pressure you into sending them money or personal information. Now, some scammers are pretending to be popular online shopping websites, phishing for your personal information.

For example, you get a call from someone who claims to be with “Amazon.com.” (Spoiler alert: they’re not really from Amazon.) The voice on the phone will say that your credit card has been charged a large amount of money for some order. Then, they’ll give you the “Amazon Support” phone number and tell you to immediately call if you didn’t make that purchase.

</Text>
                    

                  </View>
                  <View style={{marginTop: hp(2), justifyContent: 'center', alignItems: 'center', width: '100%'}}> 
                    <Text 
                      style={{...styles.modalText_, fontSize: wp(4), textDecorationLine: 'underline', fontStyle: 'italic'}}
                      onPress={() => Linking.openURL('https://www.consumer.ftc.gov/blog/2020/07/hang-business-imposter-scams')}
                    >
                        Click to visit website
                    </Text>

                  </View>

                </ScrollView>
              </SafeAreaView>
              
            </View>
            <View style = {{ ...styles.centrify_, flex: 1, backgroundColor: '#1C2B3A',  }} >
              <TouchableHighlight
                style={{ ...styles.openButton_, backgroundColor: "#00ACEB", }}
                onPress={() => {setModalArray([false,false,false,false])}}
                //disabled = {runButton}
              >
                <Text style={styles.textStyle_}>     Close     </Text>
              </TouchableHighlight>                
            </View>
          </View>
        </View>
      </Modal>

      <View style={{ ...styles.container, flex: 1}}>
        <Text style = {{ fontSize: wp(6), color: '#D3D3D3',     }}>Alerts</Text>
        {/*<Text style = {{ fontSize: windowWidth/25, color: 'black',     }}> (Verified Devices) </Text>*/}
      
      </View>

      <View style={{ ...styles.container, flex: 5 }}>
        <SafeAreaView style={{ ...styles.container,   }}>
          <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>
            

            <View  style= {{ ...styles.tableRow_, marginBottom: hp(3), borderColor: 'white', borderRadius: 20, borderWidth: 1, padding: wp(1),  justifyContent: 'center', alignItems: 'center' }}>
              <View style= {{flex: 4, margin: wp(2), borderColor: bordCol, borderWidth: bordWid,}}>
                <View style= {{flex: 1, justifyContent: 'center',}}>                  
                  <Text style={{color: 'white', fontSize: wp(4)}} numberOfLines={1}>
                    Crypto Currency Scam                    
                  </Text>        
                </View>

                <View style= {{flex: 1, justifyContent: 'center',}}>                     
                  <Text style={{color: 'white', fontSize: wp(2.5)}} numberOfLines={3}>
                    If you hold are interested in Crypto Currencies, there are number of new scams
targeting you typically through emails or social-media posts..
                  </Text>
                </View>
              </View>

              <View style= {{flex: 1.5, margin: wp(2), borderColor: bordCol, borderWidth: bordWid, flexDirection: 'row'}}>
                
                <View style = {{flex:1, justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderWidth: 0}}>
                  <TouchableOpacity
                    style={{...styles.button, alignItems: 'center', justifyContent:'center'}}
                    onPress={() => {setModalArray([true,false,false,false])}}
                  >
                    
                    <Text style = {{   color: 'white'  }}>Learn More...</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>

            <View  style= {{ ...styles.tableRow_, marginBottom: hp(3), borderColor: 'white', borderRadius: 20, borderWidth: 1, padding: wp(1),  justifyContent: 'center', alignItems: 'center' }}>
              <View style= {{flex: 4, margin: wp(2), borderColor: bordCol, borderWidth: bordWid,}}>
                <View style= {{flex: 1, justifyContent: 'center',}}>                  
                  <Text style={{color: 'white', fontSize: wp(4)}} numberOfLines={1}>
                    Medicare Beneficiaries Free Braces Scam
                  </Text>        
                </View>

                <View style= {{flex: 1, justifyContent: 'center',}}>                     
                  <Text style={{color: 'white', fontSize: wp(2.5)}} numberOfLines={3}>
If you or a someone you know have Medicare benefits, Dept. of health is alerting about
a scam to offer free orthotic braces to Medicare beneficiaries                  </Text>
                </View>
              </View>

              <View style= {{flex: 1.5, margin: wp(2), borderColor: bordCol, borderWidth: bordWid, flexDirection: 'row'}}>
                
                <View style = {{flex:1, justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderWidth: 0}}>
                  <TouchableOpacity
                    style={{...styles.button, alignItems: 'center', justifyContent:'center'}}
                    onPress={() => {setModalArray([false,true,false,false])}}
                  >
                   
                    <Text style = {{   color: 'white'  }}>Learn More...</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
            
            <View  style= {{ ...styles.tableRow_, marginBottom: hp(3), borderColor: 'white', borderRadius: 20, borderWidth: 1, padding: wp(1),  justifyContent: 'center', alignItems: 'center' }}>
              <View style= {{flex: 4, margin: wp(2), borderColor: bordCol, borderWidth: bordWid,}}>
                <View style= {{flex: 1, justifyContent: 'center',}}>                  
                  <Text style={{color: 'white', fontSize: wp(4)}} numberOfLines={1}>
                    Customer Support scams through Google search                  
                  </Text>        
                </View>

                <View style= {{flex: 1, justifyContent: 'center',}}>                     
                  <Text style={{color: 'white', fontSize: wp(2.5)}} numberOfLines={3}>
FTC Warning: Internet search for a company support phone number or site can lead to a
scammer call-center or website.                  </Text>
                </View>
              </View>

              <View style= {{flex: 1.5, margin: wp(2), borderColor: bordCol, borderWidth: bordWid, flexDirection: 'row'}}>
                
                <View style = {{flex:1, justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderWidth: 0}}>
                  <TouchableOpacity
                    style={{...styles.button, alignItems: 'center', justifyContent:'center'}}
                    onPress={() => {setModalArray([false,false,true,false])}}
                  >
                    
                    <Text style = {{   color: 'white'  }}>Learn More...</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>

            <View  style= {{ ...styles.tableRow_, marginBottom: hp(3), borderColor: 'white', borderRadius: 20, borderWidth: 1, padding: wp(1),  justifyContent: 'center', alignItems: 'center' }}>
              <View style= {{flex: 4, margin: wp(2), borderColor: bordCol, borderWidth: bordWid,}}>
                <View style= {{flex: 1, justifyContent: 'center',}}>                  
                  <Text style={{color: 'white', fontSize: wp(4)}} numberOfLines={1}>
                    Official Business Impersonation                   
                  </Text>        
                </View>

                <View style= {{flex: 1, justifyContent: 'center',}}>                     
                  <Text style={{color: 'white', fontSize: wp(2.5)}} numberOfLines={3}>
FTC Warning: Scammers pose as IRS, DMV, local police officer or other type of official to
pressure you in to sending money or share personal information.                  </Text>
                </View>
              </View>

              <View style= {{flex: 1.5, margin: wp(2), borderColor: bordCol, borderWidth: bordWid, flexDirection: 'row'}}>
                
                <View style = {{flex:1, justifyContent: 'center', alignItems: 'center', borderColor: 'white', borderWidth: 0}}>
                  <TouchableOpacity
                    style={{...styles.button, alignItems: 'center', justifyContent:'center'}}
                    onPress={() => {setModalArray([false,false,false,true])}}
                  >
                    
                    <Text style = {{   color: 'white'  }}>Learn More...</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>



          </ScrollView>
        </SafeAreaView>
      
      </View>
     
      
      

      {/*<View style={{ ...styles.container, flex: 0.75 }}>
        <Text style = {{ fontSize: windowWidth/25, textAlign: 'center', color: 'white',    }}> WhiteHax App on each Device can provide security risk remediation steps </Text>
      </View>*/}
       

    </View>

    
  );

};

function HeaderComponent(){
  return(
    
    <Text style= {{ fontSize: windowHeight/30, color:'#FFFFFF' }}>White<Text style= {{ fontSize: windowHeight/30, color:'#FE0000' }}>HaX</Text></Text>
    
  )
}
export default function Notifications({ navigation }) {
  return (

    <Stack.Navigator>

      <Stack.Screen name="Notifications" component={NotificationsScreen} options={{
          
          headerTitleStyle: {
            fontSize: windowWidth/10,

            color: 'red',
          },
          headerStyle: {
            backgroundColor: '#1C2B3A',
            height: windowHeight/7.5,
          },
          title: HeaderComponent(),
          headerLeft: () => (
            <TouchableWithoutFeedback  onPress={() => navigation.toggleDrawer()}>
              <View style = {{ paddingLeft: windowWidth/30, }}>
                <Image
                  style={{ width: 35, height: 35, }}
                  source={require('../assets/android/4x/top_bar_menu_iconxxxhdpi.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            
          ),
        }}/>
      
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18222E' 
  },
  centrify:{
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  customRoww: {
        flex: 1,
        justifyContent:'center',

        flexDirection: 'row',
       
        
        
        //marginTop: windowHeight/200,
        //marginBottom: windowHeight/200,
        borderRadius: 0,
        backgroundColor: '#18222E',
        elevation: 2,


    },
    
    customRoww_text: {

        //paddingBottom: windowHeight/400,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },
    firstCustomRowText: {

        //paddingBottom: windowHeight/20,
        //paddingTop: windowHeight/20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },

    category: {
        fontSize:wp(3.5) ,
        color: 'white',
              
        
    },
    status: {
        
        fontSize:15,
        
        color: 'white', 
             
    },
    risk: {
      
        fontSize: wp(3),
        
        color: 'white', 
        textAlign: 'center',
             
    },
    //modal stuff -->
    centrifyy:{
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },

    button: {
      alignItems: "center",
      
      
    },

    //<-- modal stuff

    body: {
    flex: 1,
    padding: 10, 
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  





  tableRow_: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      flexDirection: 'row',
      width: '100%',
      height: '100%',
    },
    centrify_:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  
  centeredView_: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    
  },
  modalView_: {
    flex: 1,
    width: wp(75),
    margin: hp(10),
    backgroundColor: "#1C2B3A",
    borderRadius: 20,
    padding: wp(5),
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 50,
      height: 52
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton_: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: wp(3),
    elevation: 2
  },
  textStyle_: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    
    
         
  },
  modalText_: {
    
    textAlign: "center",
    fontSize: wp(5),
    color: '#D3D3D3'
         
  },
  
});