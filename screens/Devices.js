import React, { useRef, useState, useEffect, Component, useContext } from "react";
import { FlatList, Animated, Modal, Button, View, Text, Image, TouchableWithoutFeedback, StyleSheet, TouchableOpacity, TouchableHighlight,SafeAreaView, ScrollView  } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator} from '@react-navigation/stack';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AppContext from "../components/AppContext";

//import { useFonts } from '@use-expo/font';

//import { AppLoading } from 'expo';
//import { Table, TableWrapper, Cell, Row, Rows } from 'react-native-table-component';

const Stack = createStackNavigator();
import {NativeModules} from 'react-native';



var TestModule = NativeModules.TestModule;





import { Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
let bordCol = "#00ACEB";
let bordWid = 1




function DevicesScreen({navigation}) {

  // let [fontsLoaded] = useFonts({
  //   'DidactGothic-Regular': require('../assets/fonts/DidactGothic-Regular.ttf'),
  // });
  const myContext = useContext(AppContext);

  let i;
  const [tempData, setTempData] = useState('n/a')

  
  
  
  
  
  return (
    

    <View style={{ ...styles.container, flex: 1, }}>
      
      

      <View style={{ ...styles.container, flex: 0.5}}>
        <Text style = {{ fontSize: wp(6), color: '#D3D3D3',     }}> Current Security Status </Text>
        {/*<Text style = {{ fontSize: windowWidth/25, color: 'black',     }}> (Verified Devices) </Text>*/}
      
      </View>

      <View style={{ ...styles.container, flex: 0.5, }}>
        <View style = {{ flexDirection: 'row', borderBottomWidth: 1, borderColor: '#00ACEB', marginLeft: wp(1), marginRight: wp(1), paddingBottom: hp(1),}}>
          <View style = {{ flex: 1.5, }}>
            <Text style = {{ fontSize: wp(4), color: '#00ACEB'}}>Device Name</Text>
          </View>

          <View style = {{ flex: 2, }}>
            <Text style = {{ fontSize: wp(4), color: '#00ACEB', textAlign: 'center'}}>Readiness</Text>
          </View>

          <View style = {{ flex: 1, }}>
            <Text style = {{ fontSize: wp(4), color: '#00ACEB', textAlign: 'center'}}>Risk</Text>
          </View>
        </View>
      </View>

      <View style={{ ...styles.container, flex: 4 }}>
        <SafeAreaView style={{ ...styles.container,   }}>
          <ScrollView style={{ flex: 1, width: '100%', height: '100%'}}>
            <View style= {{ alignItems: 'center', justifyContent: 'center', flex: 1, flexDirection: 'row', width: '100%', height: '100%', paddingTop: hp(2.5), paddingBottom: hp(3), paddingLeft: wp(1), paddingRight: wp(1) }}>

              <View style = {{ flex: 1.5, }}>
                <Text style={{flex: 1, color: 'white', fontSize: wp(4)}} numberOfLines={1}>This Device</Text>
              </View>

              <View style = {{ flex: 2, paddingRight: wp(2), paddingLeft: wp(1)}}>
                <View style = {{ height: '100%', borderWidth: 0.25, borderRadius: 20, borderColor: '#00ACEB' }}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: myContext.ReadinessGeneralPercentValue > 15 ? myContext.ReadinessGeneralPercentValue.toString()+'%' : '15%', backgroundColor: myContext.ReadinessGeneralPercentValue > 90 ? '#2E7D32' : myContext.ReadinessGeneralPercentValue > 70 ? '#E65100' : myContext.ReadinessGeneralPercentValue > 40 ? '#F57F17' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(2.5)}}>{myContext.ReadinessGeneralPercentValue}%</Text>
                  </View>
                </View>
              </View>

              <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <View style = {{ justifyContent: 'center', alignItems: 'center', height: '100%', width: '75%', backgroundColor: myContext.ReadinessGeneralPercentValue > 90 ? '#2E7D32' : myContext.ReadinessGeneralPercentValue > 70 ? '#E65100' : myContext.ReadinessGeneralPercentValue > 40 ? '#F57F17' : '#B71C1C', borderWidth: 0, borderRadius: 20,  }}>
                    <Text style = {{color: 'white', fontSize: wp(3.5)}}>{myContext.ReadinessGeneralPercentValue > 90 ? 'Low' : myContext.ReadinessGeneralPercentValue > 70 ? 'Med' : myContext.ReadinessGeneralPercentValue > 40 ? 'High' : 'Crit'}</Text>
                  </View>
              </View>

            </View>
            
            {myContext.DeviceLoopValue}
            

            


            

            






          </ScrollView>
        </SafeAreaView>
      
      </View>
     
      
      

      {/*<View style={{ ...styles.container, flex: 0.75 }}>
        <Text style = {{ fontSize: windowWidth/25, textAlign: 'center', color: 'white',    }}> WhiteHax App on each Device can provide security risk remediation steps </Text>
      </View>*/}
      

    </View>

    
  );

};

function HeaderComponent(){
  return(
    
    <Text style= {{ fontSize: windowHeight/30, color:'#FFFFFF' }}>White<Text style= {{ fontSize: windowHeight/30, color:'#FE0000' }}>HaX</Text></Text>
    
  )
}
export default function Devices({ navigation }) {
  return (

    <Stack.Navigator>

      <Stack.Screen name="Devices" component={DevicesScreen} options={{
          
          headerTitleStyle: {
            fontSize: windowWidth/10,

            color: 'red',
          },
          headerStyle: {
            backgroundColor: '#1C2B3A',
            height: windowHeight/7.5,
          },
          title: HeaderComponent(),
          headerLeft: () => (
            <TouchableWithoutFeedback  onPress={() => navigation.toggleDrawer()}>
              <View style = {{ paddingLeft: windowWidth/30, }}>
                <Image
                  style={{ width: 35, height: 35, }}
                  source={require('../assets/android/4x/top_bar_menu_iconxxxhdpi.png')}
                />
              </View>
            </TouchableWithoutFeedback>
            
          ),
        }}/>
      
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  container:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18222E' 
  },
  centrify:{
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
//table -->

  tableRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    height: '100%',
  },

  //<-- table


  customRoww: {
        flex: 1,
        justifyContent:'center',

        flexDirection: 'row',
       
        
        
        //marginTop: windowHeight/200,
        //marginBottom: windowHeight/200,
        borderRadius: 0,
        backgroundColor: '#18222E',
        elevation: 2,


    },
    
    customRoww_text: {

        //paddingBottom: windowHeight/400,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },
    firstCustomRowText: {

        //paddingBottom: windowHeight/20,
        //paddingTop: windowHeight/20,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        flex: 1,
        flexDirection: 'row',
        
        justifyContent: 'center',
        borderBottomWidth: 0,
    },

    category: {
        fontSize:wp(3.5) ,
        color: 'white',
              
        
    },
    status: {
        
        fontSize:15,
        
        color: 'white', 
             
    },
    risk: {
      
        fontSize: wp(3),
        
        color: 'white', 
        textAlign: 'center',
             
    },
    //modal stuff -->
    centrifyy:{
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },

    button: {
      alignItems: "center",
      
      
    },

    //<-- modal stuff
  
});